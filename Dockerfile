ARG IMAGE_VERSION=latest
ARG IMAGE_NAME=registry.gitlab.com/cabildo-tf/vente/templates/static-webapps/v-1-3

# hadolint ignore=DL3007
FROM ${IMAGE_NAME}:${IMAGE_VERSION}

ARG NGINX_ALIAS="/data"

ENV NGINX_ALIAS="${NGINX_ALIAS}"

LABEL maintainer="ilorgar@gesplan.es"

#COPY dist/buildOutput/app "${NGINX_ALIAS}"
COPY src "${NGINX_ALIAS}"
