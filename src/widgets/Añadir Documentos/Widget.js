define(['jimu/dijit/FeaturelayerChooserFromMap', "esri/geometry/Point", "esri/SpatialReference", 'jimu/dijit/LoadingShelter', "dojo/dom-class", 'dojo/on', "esri/map",
	'jimu/dijit/LayerChooserFromMapWithDropbox', 'dojo/_base/declare', "esri/dijit/LayerList", "esri/arcgis/utils", 'jimu/BaseWidget', 'jimu/BaseWidgetSetting', 'dojo/_base/lang', 'jimu/LayerInfos/LayerInfos', 'dojo/_base/array', "dojo/dom-attr",
	"esri/toolbars/draw", 'esri/layers/GraphicsLayer', "esri/layers/FeatureLayer", "esri/tasks/QueryTask", "esri/tasks/query", "esri/graphic",
	'esri/geometry/Point', 'esri/geometry/Polyline', 'esri/symbols/SimpleLineSymbol', 'esri/symbols/SimpleFillSymbol', 'esri/symbols/SimpleMarkerSymbol', 'esri/Color', "esri/tasks/GeometryService", "esri/tasks/BufferParameters", 'esri/dijit/editing/AttachmentEditor',"dojo/parser","esri/geometry/Extent","dojo/dom-construct","dojo/domReady!"],
	function (FeaturelayerChooserFromMap, Point, SpatialReference, LoadingShelter, dojoClass, on, Map,
		LayerChooserFromMapWithDropbox, declare, LayerList, arcgisUtils, BaseWidget, BaseWidgetSetting, lang, LayerInfos, array, domAttr, Draw, GraphicsLayer, FeatureLayer, QueryTask, Query, Graphic, Point, Polyline, SimpleLineSymbol, SimpleFillSymbol, SimpleMarkerSymbol, Color, GeometryService, BufferParameters, AttachmentEditor, Parser, Extent, DomConstruct) {
		return declare([BaseWidget], {
			baseClass: 'jimu-widget-customwidget',
			layerEntidades: [],
			layerRelations: [],
			layerGeometria: [],
			tablaPos_base: [],
			tablaPos_ini: [],
			tablaPos_fin: [],
			layerTablas: [],
			cont: 0,
			DomRegulacion: [],
			DomLimitacion: [],
			entidadEq: "",
			id_eq: "",	
			url_eq: "",			
			url_relation: "",
			url_doc: "",
			url_red: "",
			url_acceso: "",							
			title_doc: "Documentos",	
			elementos_eq: [],
			elementos_aux: [],		
			selected_eq: [],
			selected_eq_f: [],			
			geometry_eq: "",
			draw: null,
			doc_nombre: "",
			doc_descricion: "",
			doc_idioma: "",
			doc_uso_interno: "",
			doc_editorial: "",
			doc_visible: "",
			doc_codigo_anterior: "",
			doc_nombre_anterior: "",
			doc_objectid: "",
			Graphics_eqd: null,
			graphics_globalid_doc: [],
			marcar_todos: false,			
			onOpen: function () {

			},
			onClose: function () {
				this.draw.deactivate();
				if (document.getElementById('end').style.display == 'block') {
					document.getElementById('dialogo_exit').style.display = 'none';
					this.go_inicio();
				} else if (document.getElementById('inicio').style.display == 'block') {
					document.getElementById('dialogo_exit').style.display = 'none';
				} else {
					document.getElementById('dialogo_exit').style.display = 'block';
				}
			},

			startup: function () {
				gsvc = new GeometryService(this.config.GeometryServer);
				this.Graphics_eqd = new GraphicsLayer({"id": 'glEquipamientosDoc'});
      	this.map.addLayer(this.Graphics_eqd);
				Parser.parse();				        
				this.inherited(arguments);
				var NombreEntidades = [];
				var NombreRelations = [];
				var Idioma = [];
				var IdiomaValue = [];
				var Booleano = [];
				var BooleanoValue = [];	
				
				
				//Dominio Regulacion				
				//Obtener los keys de las entidades del Config				
				for (const [key, value] of Object.entries(this.config.Regulacion)) {
					this.DomRegulacion[value]=key;;					
				}	
				
				//Dominio Limitacion				
				//Obtener los keys de las entidades del Config				
				for (const [key, value] of Object.entries(this.config.Limitacion)) {
					this.DomLimitacion[value]=key;;					
				}		
				
				//Llamar al div del combo
				let divCombo = document.getElementById("selec_entidad");
				//Obtener los keys de las entidades del Config				
				for (const [key, value] of Object.entries(this.config.Capas)) {
					NombreEntidades.push(key);
					NombreRelations.push(value);
				}
				//Rellenar el combo con los keys
				for (i = 0; i < NombreEntidades.length; i++) {
					divCombo.innerHTML += '<option value="' + NombreEntidades[i] + '">' + NombreEntidades[i] + '</option>';
				}
				
				//Llamar al div del combo de Idiomas
				let divIdioma = document.getElementById("idioma_doc");
				//Obtener los keys de los idiomas del Config
				for (const [key, value] of Object.entries(this.config.Idioma)) {
					Idioma.push(key);
					IdiomaValue.push(value);				
				}
				//Rellenar el combo con los Idiomas
				for (i = 0; i < Idioma.length; i++) {
					if (IdiomaValue[i] == 38){
				  	divIdioma.innerHTML += '<option value="' + IdiomaValue[i] + '" selected>' + Idioma[i] + '</option>';
				  } else {
						divIdioma.innerHTML += '<option value="' + IdiomaValue[i] + '">' + Idioma[i] + '</option>';
				  }
				}
				
				//Llamar al div del combo de Visible y Uso_interno
				let divInterno = document.getElementById("uso_interno_doc");
				let divVisible = document.getElementById("visible_doc");
				//Obtener los keys de los idiomas del Config
				for (const [key, value] of Object.entries(this.config.Booleano)) {
					Booleano.push(key);
					BooleanoValue.push(value);				
				}
				//Rellenar el combo con Booleano
				for (i = 0; i < Booleano.length; i++) {
					divInterno.innerHTML += '<option value="' + BooleanoValue[i] + '">' + Booleano[i] + '</option>';
					divVisible.innerHTML += '<option value="' + BooleanoValue[i] + '">' + Booleano[i] + '</option>';					
				}
				
				//Entidades relacionadas con documentos
				this.cont = 0;
				for (i = 0; i < NombreEntidades.length; i++) {
					for (j = 0; j < this.map.itemInfo.itemData.operationalLayers.length; j++) {
						if (this.map.itemInfo.itemData.operationalLayers[j].title == NombreEntidades[i]) {
							this.layerEntidades[this.cont] = new this.Relations(this.map.itemInfo.itemData.operationalLayers[j].url, this.map.itemInfo.itemData.operationalLayers[j].id,this.map.itemInfo.itemData.operationalLayers[j].title);
							this.layerGeometria.push(this.map.itemInfo.itemData.operationalLayers[j].title);
							this.cont++;
						}
					}
					for (j = 0; j < this.map.itemInfo.itemData.tables.length; j++) {
						if (this.map.itemInfo.itemData.tables[j].title == NombreEntidades[i]) {
							this.layerEntidades[this.cont] = new this.Relations(this.map.itemInfo.itemData.tables[j].url, this.map.itemInfo.itemData.tables[j].id,this.map.itemInfo.itemData.tables[j].title);
							this.layerTablas.push(this.map.itemInfo.itemData.tables[j].title);
							this.cont++;
						}
					}
				}
				
				//Tablas N-M relacionados con documentos
				this.cont = 0;
				for (i = 0; i < NombreRelations.length; i++) {
					for (j = 0; j < this.map.itemInfo.itemData.tables.length; j++) {
						if (this.map.itemInfo.itemData.tables[j].title == NombreRelations[i]) {
							this.layerRelations[this.cont] = new this.Relations(this.map.itemInfo.itemData.tables[j].url, this.map.itemInfo.itemData.tables[j].id,this.map.itemInfo.itemData.tables[j].title);
							this.cont++;
						}
					}
				}	
				
				//URL de Redes recreativas
				for (j = 0; j < this.map.itemInfo.itemData.tables.length; j++) {
					if (this.map.itemInfo.itemData.tables[j].title == this.config.Red_recreativa) {
						this.url_red = this.map.itemInfo.itemData.tables[j].url;
						break;
					}
				}
				
				//URL de Accesos
				for (j = 0; j < this.map.itemInfo.itemData.tables.length; j++) {
					if (this.map.itemInfo.itemData.tables[j].title == this.config.Grupo_instalacion_recreativa_acceso) {
						this.url_acceso = this.map.itemInfo.itemData.tables[j].url;
						break;
					}
				}
				
				//URL de Documento
				for (i = 0; i < this.map.itemInfo.itemData.tables.length; i++) {
					if (this.map.itemInfo.itemData.tables[i].title == this.title_doc) {
						this.url_doc = this.map.itemInfo.itemData.tables[i].url;
					}
				}			

				//Boton Comenzar
				var button_fc = document.getElementById('comenzar');
				button_fc.addEventListener('click', () => {					
					this.load_page(1);
				});
				
				//Button Combo Tipo Entidad	
				var combobutton = document.getElementById("selec_entidad");
		    combobutton.addEventListener("change", () => {
		    	this.Graphics_eqd.clear();
		    	document.getElementById('combo_one_list').style.display = 'none';
		    	document.getElementById('select_polygon').style.display = 'none';
		      document.getElementById('buscador').style.display = 'none';
		      document.getElementById('entidad_list').style.display = 'none';
		      document.getElementById('geometry_oblig').style.display = 'none';
				  document.getElementById('select_region').style.display = 'none';
					document.getElementById('least_one_list').style.display = 'none';
					document.getElementById('layer_novisible').style.display = 'none';
					document.getElementById('layer_nogeometria').style.display = 'none';
		      document.getElementById('option1').checked = '';
		    	document.getElementById('option2').checked = '';
		    	document.getElementById('option1').disabled = false;		    	
		    	var EntidadSeleccionada = document.getElementById("selec_entidad").value;
		    	if (this.layerTablas.includes(EntidadSeleccionada) == true){ 
		       document.getElementById('option1').disabled = true;
		       document.getElementById('layer_nogeometria').style.display = 'block';
		      }
		      if (EntidadSeleccionada == this.config.format_red_recreativa_regulacion){
		      	document.getElementById('buscador_text').placeholder = "Introduzca matricula o nombre red recreativa..." ;
		      } else if (EntidadSeleccionada == this.config.format_grupo_instalacion_recreativa_acceso_regulacion){
		      	document.getElementById('buscador_text').placeholder = "Introduzca nombre acceso..." ;
		      } else {
		      	document.getElementById('buscador_text').placeholder = "Introduzca codigo o nombre..." ;
		      }		      
		    });
				
				//Button TipoBusqueda	
				var tipomapabutton = document.getElementById("option1");
		    tipomapabutton.addEventListener("click", () => {
		    	document.getElementById('buscador_text').value="";
		    	document.getElementById('no_list').style.display = 'none';
		    	var EntidadSeleccionada = document.getElementById("selec_entidad").value;
					if (EntidadSeleccionada == "") {
						 document.getElementById('combo_one_list').style.display = 'block';
						 document.getElementById('option1').checked = '';
						 document.getElementById('geometry_oblig').style.display = 'none';
						 document.getElementById('select_region').style.display = 'none';
						 document.getElementById('least_one_list').style.display = 'none';
						 document.getElementById('layer_novisible').style.display = 'none';						 
				  } else {
				  	 this.Graphics_eqd.clear();
				  	 document.getElementById('geometry_oblig').style.display = 'none';
						 document.getElementById('select_region').style.display = 'none';
						 document.getElementById('least_one_list').style.display = 'none';
						 document.getElementById('layer_novisible').style.display = 'none';
		         document.getElementById('select_polygon').style.display = 'block';
		         document.getElementById('buscador').style.display = 'none';		         
		         document.getElementById('entidad_list').innerHTML = '';
		         document.getElementById('entidad_list').style.display = 'none';		         
		      }
		    });
											
				var tipobusquedabutton = document.getElementById("option2");
		    tipobusquedabutton.addEventListener("click", () => {
		    	var EntidadSeleccionada = document.getElementById("selec_entidad").value;
					if (EntidadSeleccionada == "") {
						 document.getElementById('combo_one_list').style.display = 'block';
						 document.getElementById('option2').checked = '';
						 document.getElementById('geometry_oblig').style.display = 'none';
						 document.getElementById('select_region').style.display = 'none';
						 document.getElementById('least_one_list').style.display = 'none';
						 document.getElementById('layer_novisible').style.display = 'none';
						 document.getElementById('layer_nogeometria').style.display = 'none';
				  } else {
				  	 this.Graphics_eqd.clear();
				  	 document.getElementById('geometry_oblig').style.display = 'none';
						 document.getElementById('select_region').style.display = 'none';
						 document.getElementById('least_one_list').style.display = 'none';
						 document.getElementById('layer_novisible').style.display = 'none';
		         document.getElementById('select_polygon').style.display = 'none';
		         document.getElementById('layer_nogeometria').style.display = 'none';
		         document.getElementById('buscador').style.display = 'flex';
		         document.getElementById('entidad_list').innerHTML = '';
		         this.rellenar_entidades();		         
		      }  
		    });
		    
		    //Boton Draw
				var draw_btn = document.getElementById('select_polygon');
				draw_btn.onclick = lang.hitch(this, this.botonDraw);				
				
				this.draw = new Draw(this.map);
				this.draw.on('draw-complete', lang.hitch(this, this.geoEquipamiento));
		    
		    //Text Buscador
				var buscador_f1 = document.getElementById('buscador_text');
				buscador_f1.addEventListener('input', () => {
					this.rellenar_entidades();
					this.marcar_todos = false;
				});	
				
				//Boton Seleccionar Todos
				var b_select_all = document.getElementById('select_all');
				b_select_all.addEventListener('click', () => {
					if (this.marcar_todos == false){
						for (var i = 0; i < this.elementos_eq.length; i++) {
						  var n = i.toString();
						  var id = 'check_btn_' + n;					  
						 	if (document.getElementById(id) != null) {
									document.getElementById(id).checked = true;
						  }
						  this.marcar_todos = true;
				  	}						
					} else {
						for (var i = 0; i < this.elementos_eq.length; i++) {
						  var n = i.toString();
						  var id = 'check_btn_' + n;					  
						 	if (document.getElementById(id) != null) {
									document.getElementById(id).checked = false;
						  }
						  this.marcar_todos = false;					
				  	}
					}
				});	

				//Boton Siguiente Formulario_1
				var button_f1 = document.getElementById('end_form_1');
				button_f1.addEventListener('click', () => {
				  this.selected_eq = [];
				  for (var i = 0; i < this.elementos_eq.length; i++) {
					  var n = i.toString();
					  var id = 'check_btn_' + n;					  
					 	if (document.getElementById(id) != null && document.getElementById(id).checked == true) {
								this.selected_eq.push(this.elementos_eq[i]);
					  }						
				  }				  
				  if (this.selected_eq.length == 0) {
					  document.getElementById('least_one_list').style.display = 'block';			  
				  }
				  else {				  	
					  document.getElementById('least_one_list').style.display = 'none';	
					  document.getElementById('least_one_list_select').style.display = 'none';		  
					  this.load_page(2);
					  var EntidadSeleccionada = document.getElementById("selec_entidad").value;
					  this.entidadEq = EntidadSeleccionada;
		    		if (this.layerGeometria.includes(EntidadSeleccionada) == true){
		    			this.Graphics_eqd.clear();
		    			var globalid_selected ="globalid IN (";
				      for (i = 0; i < this.selected_eq.length; i++) {        
				        globalid_selected += "'"+this.selected_eq[i].features.attributes.globalid+"',"; 
				      }
					    globalid_selected += "'')";
					    var query = new Query();							
							query.where = globalid_selected;
							query.outFields = ["*"];
							query.returnGeometry = true;
							this.map._layers[this.id_eq].selectFeatures(query,FeatureLayer.SELECTION_NEW,(result_select_graphics) => {									
								  for (var i = 0; i < result_select_graphics.length; i++) {
								  	if (this.config.capas_lineales.includes(this.entidadEq)){
								  		var line = new SimpleLineSymbol();
								  		line.setWidth(5);
											line.setColor(new Color([0, 197, 255, 1]));
											result_select_graphics[i].symbol = line;
									  	this.Graphics_eqd.add(result_select_graphics[i]);
										  if(i == result_select_graphics.length - 1){
											this.Graphics_eqd.clear();
											for (var i = 0; i < result_select_graphics.length; i++) {
												this.Graphics_eqd.add(result_select_graphics[i]);
											}
										}
								  	} else {
									  	var line = new SimpleLineSymbol();
											line.setColor(new Color([0, 197, 255, 1]));
											var marker = new SimpleMarkerSymbol();
											marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
											marker.setColor(new Color([0, 197, 255, 0.26]));
											marker.setAngle(0);
											marker.setOutline(line);
									  	result_select_graphics[i].symbol = marker;
									  	this.Graphics_eqd.add(result_select_graphics[i]);
									  }
								  }
								  this.Graphics_eqd.redraw();		    	
							}
							, (rollback_select_graphics) => {
										this.Graphics_eqd.clear();
							});			
		        	this.load_table_selected("CodigoNombre");
		        } else if (EntidadSeleccionada == this.config.format_red_recreativa_sistema_senalizacion) {
		        	this.load_table_selected("CodigoNombre");
		        } else if (EntidadSeleccionada == this.config.format_red_recreativa_regulacion){
		        	this.load_table_selected(this.config.format_red_recreativa_regulacion);
		      	} else if (EntidadSeleccionada == this.config.format_grupo_instalacion_recreativa_acceso_regulacion){
		        	this.load_table_selected(this.config.format_grupo_instalacion_recreativa_acceso_regulacion);
		      	} else {
		      		this.load_table_selected("globalid");
		        }
				  }          
				});							

				//Boton Siguiente Formulario_2
				var button_f2 = document.getElementById('end_form_23');
				button_f2.addEventListener('click', () => {										
					this.selected_eq_f = [];
					for (var i = 0; i < this.selected_eq.length; i++) {
						var n = i.toString();
						var id = 'check_btn_select_' + n;
						if (document.getElementById(id).checked == true) {
							this.selected_eq_f.push(this.selected_eq[i]);
						}
					}
					if (this.selected_eq_f.length == 0) {
						document.getElementById('least_one_list_select').style.display = 'block';							
					}
					else {
						document.getElementById('least_one_list_select').style.display = 'none';
						this.load_page(3);
					}					
				});

				//Boton Atras Formulario_2
				document.getElementById('end_form_21').addEventListener('click', () => {					
					document.getElementById('least_one_list').style.display = 'none';
					this.load_page(1);
					if (document.getElementById('option2').checked == true){
						this.Graphics_eqd.clear();	
						this.tablaPos_base = [];
											
					} 
					if (document.getElementById('option1').checked == true){
						this.tablaPos_base = this.tablaPos_ini;						
						var globalid_selected ="globalid IN (";
			      for (i = 0; i < this.elementos_eq.length; i++) {			      	    
			        	globalid_selected += "'"+this.elementos_eq[i].features.attributes.globalid+"',"; 			        
			      }
				    globalid_selected += "'')";
				    var query = new Query();							
						query.where = globalid_selected;
						query.outFields = ["*"];
						query.returnGeometry = true;
						this.graphics_globalid_doc = [];
						for (var l = 0; l < this.Graphics_eqd.graphics.length; l++){
							this.graphics_globalid_doc.push(this.Graphics_eqd.graphics[l].attributes.globalid);
						}
						this.map._layers[this.id_eq].selectFeatures(query,FeatureLayer.SELECTION_NEW,(result_select_graphics) => {
								for (var i = 0; i < result_select_graphics.length; i++) {
								if (this.Graphics_eqd.graphics[i].geometry.__proto__.declaredClass == 'esri.geometry.Polyline'){
							  		if (this.graphics_globalid_doc.includes(result_select_graphics[i].attributes.globalid)== false){
											result_select_graphics[i].symbol = null;
									  	this.Graphics_eqd.add(result_select_graphics[i]);
									  } else {
									  	var line = new SimpleLineSymbol();
								  		line.setWidth(5);
											line.setColor(new Color([0, 197, 255, 1]));
											result_select_graphics[i].symbol = line;
									  	this.Graphics_eqd.add(result_select_graphics[i]);
										}
							  	} else {
							  		if (this.graphics_globalid_doc.includes(result_select_graphics[i].attributes.globalid)== false){
									  	result_select_graphics[i].symbol = null;
									  	this.Graphics_eqd.add(result_select_graphics[i]);
									  } else{
									  	var line = new SimpleLineSymbol();
											line.setColor(new Color([0, 197, 255, 1]));
											var marker = new SimpleMarkerSymbol();
											marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
											marker.setColor(new Color([0, 197, 255, 0.26]));
											marker.setAngle(0);
											marker.setOutline(line);
									  	result_select_graphics[i].symbol = marker;
									  	this.Graphics_eqd.add(result_select_graphics[i]);
										}
								  }
							  }									
							  this.Graphics_eqd.redraw();		    	
						}
						, (rollback_select_graphics) => {
									this.Graphics_eqd.clear();
						});					
					}
				});
				
				//Boton Siguiente Formulario_3
				var button_f3 = document.getElementById('end_form_34');
				button_f3.addEventListener('click', () => {						
					  var aux = false;
					  this.doc_nombre = document.getElementById('nombre_doc').value;
						this.doc_descricion = document.getElementById('descripcion_doc').value;
						this.doc_idioma = document.getElementById('idioma_doc').value;
						this.doc_uso_interno = document.getElementById('uso_interno_doc').value;
						this.doc_editorial = document.getElementById('editorial_doc').value;
						this.doc_visible = document.getElementById('visible_doc').value;
						this.doc_codigo_anterior = document.getElementById('codigo_anterior_doc').value;
						this.doc_nombre_anterior = document.getElementById('nombre_anterior_doc').value;	          
	          document.getElementById('nombre_oblig').style.display = 'none';	          
	          if (this.doc_nombre == '') {
	            document.getElementById('nombre_oblig').style.display = 'block';
	            aux = true;
	          }
	          if (this._uploadField.files.length == 0) {
	            document.getElementById('select_fichero').style.display = 'block';
	            aux = true;
	          }	          	          
	          if (aux == false) {
	          	document.getElementById('select_fichero').style.display = 'none';
	            document.getElementById('nombre_oblig').style.display = 'none';
	            this.load_page(4);
	          }							
				});
				
				//Boton Atras Formulario_3
				document.getElementById('end_form_32').addEventListener('click', () => {
					this.load_page(2);
				});

				//Boton Finalizar y Guardar Formulario 4
				var save_button = document.getElementById('end_form_44');
				save_button.addEventListener('click', () => {
					document.getElementById('btn_at_fi_4').style.display = 'none';
					this.create_new_relacion();
				});

				//Boton Atras Formulario_4
				document.getElementById('end_form_43').addEventListener('click', () => {
					this.load_page(3);
				});

				//Boton Volver al inicio
				var end_btn = document.getElementById('end');
				end_btn.addEventListener('click', () => {
					this.go_inicio();
				});
				
				//Botones dialogo de salida
				document.getElementById('confirm_button_exit').addEventListener('click', () => {
					document.getElementById('dialogo_exit').style.display = 'none';
					this.go_inicio();
				});
				document.getElementById('cancel_button_exit').addEventListener('click', function () {
					document.getElementById('dialogo_exit').style.display = 'none';
				});

			},

			rellenar_entidades: function () {
				//Obtener la entidad seleccionada
				var EntidadSeleccionada = document.getElementById("selec_entidad").value;
				document.getElementById('entidad_list').style.display = 'none';
				document.getElementById('no_list').style.display = 'none';
				document.getElementById('entidad_list').innerHTML = '';
				var resultados = [];
				var consultas = [];				
				this.elementos_eq = [];
				var url_capa = "";
				var pos_entidad = 0;
				for (var i = 0; i < this.layerEntidades.length; i++) {
			  	if (this.layerEntidades[i].related_layers_title == EntidadSeleccionada) {
			  		url_capa=this.layerEntidades[i].related_layers_url;			
			  		id_capa=this.layerEntidades[i].related_layers_id;  		
			  		pos_entidad=i;
			  		break;
			  	}
			  }
			  this.url_eq=url_capa;
			  this.id_eq = id_capa;
			  this.url_relation = this.layerRelations[pos_entidad].related_layers_url;
			  
			  if (this.layerGeometria.includes(EntidadSeleccionada) == true){
			  	if (this.map._layers[this.layerEntidades[pos_entidad].related_layers_id].visible == true) {
							var query = new Query();
							var valor = document.getElementById('buscador_text').value.toUpperCase();
							if (valor == '') {
								query.where = "1=1";
							} else {
								valor = "%" + valor + "%";
								query.where = "(UPPER(codigo) LIKE '" + valor + "' OR UPPER(nombre) LIKE '" + valor + "')";
							}
							query.outFields = ["*"];
							query.orderByFields = ["codigo", "nombre"];
							query.returnGeometry = true;
							var query_elements = new QueryTask(url_capa);
							consultas.push(query_elements.execute(query));

							Promise.all(consultas).then(results => {
								var all_items = [];
								for (var i = 0; i < results.length; i++) {
									resultados.push(results[i].features);
								}
								for (var i = 0; i < resultados.length; i++) {
									for (var j = 0; j < resultados[i].length; j++) {
										all_items.push(new this.Entidad(resultados[i][j], i));
									}
								}
								this.elementos_eq = all_items;
								this.load_table_buscador("CodigoNombre");
							}).catch(error => {
								document.getElementById('entidad_list').innerHTML = '<p class="t2_red">Se produjo un error, al cargar los datos</p>';
							})	
					} else {						
						document.getElementById('layer_novisible').style.display = 'block';
					}
			  } else {
			  	var query = new Query();
			  	if (EntidadSeleccionada == this.config.format_red_recreativa_sistema_senalizacion){
			  			var query = new Query();
			  			var valor = document.getElementById('buscador_text').value.toUpperCase();
							if (valor == '') {
								query.where = "1=1";
							} else {
								valor = "%" + valor + "%";
								query.where = "(UPPER(codigo) LIKE '" + valor + "' OR UPPER(nombre) LIKE '" + valor + "')";
							}
							query.outFields = ["*"];
							query.orderByFields = ["codigo", "nombre"];
							var query_elements = new QueryTask(url_capa);
							consultas.push(query_elements.execute(query));

							Promise.all(consultas).then(results => {
								var all_items = [];
								for (var i = 0; i < results.length; i++) {
									resultados.push(results[i].features);
								}
								for (var i = 0; i < resultados.length; i++) {
									for (var j = 0; j < resultados[i].length; j++) {
										all_items.push(new this.Entidad(resultados[i][j], i));
									}
								}
								this.elementos_eq = all_items;
								this.load_table_buscador("CodigoNombre");
							}).catch(error => {
								document.getElementById('entidad_list').innerHTML = '<p class="t2_red">Se produjo un error, al cargar los datos</p>';
							})
			    } else if (EntidadSeleccionada == this.config.format_red_recreativa_regulacion){			    		
							var query = new Query();
							query.where = "1=1";
							query.outFields = ["*"];
							query.orderByFields = ["regulacion_id"];							
							var query_elements = new QueryTask(url_capa);
							consultas.push(query_elements.execute(query));

							Promise.all(consultas).then(results => {
								var all_items = [];
								for (var i = 0; i < results.length; i++) {
									resultados.push(results[i].features);
								}
								for (var i = 0; i < resultados.length; i++) {
									for (var j = 0; j < resultados[i].length; j++) {
										all_items.push(new this.Entidad(resultados[i][j], i));
									}
								}
								this.elementos_eq = all_items;
								var resultados_red = [];
								var consultas_red = [];
								var query_where_red="";
								var valor = document.getElementById('buscador_text').value.toUpperCase();
								var globalid_red ="globalid IN (";
					      for (i = 0; i < this.elementos_eq.length; i++) {        
					        globalid_red += "'"+this.elementos_eq[i].features.attributes.red_recreativa_id+"',"; 
					      }
						    globalid_red += "'')";
						    if (valor != "") {
							      valor = "%" + valor + "%";
							      query_where_red += "( UPPER(nombre) LIKE '" + valor + "' OR UPPER(matricula) LIKE '" + valor + "') AND ";
							    }
								if (query_where_red == ""){
									query_where_red = globalid_red;
								} else {
									query_where_red += globalid_red;
								}
								var query_red = new Query();
								query_red.where = query_where_red;
								query_red.outFields = ["*"];							
								var query_elements_red = new QueryTask(this.url_red);
								consultas_red.push(query_elements_red.execute(query_red));
								Promise.all(consultas_red).then(results => {
									var all_items_red = [];
									for (var i = 0; i < results.length; i++) {
										resultados_red.push(results[i].features);
									}
									for (var i = 0; i < resultados_red.length; i++) {
										for (var j = 0; j < resultados_red[i].length; j++) {
											all_items_red.push(new this.Entidad(resultados_red[i][j], i));
										}
									}
									this.elementos_aux = all_items_red;
									this.load_table_buscador(this.config.format_red_recreativa_regulacion);
								}).catch(error => {
									document.getElementById('entidad_list').innerHTML = '<p class="t2_red">Se produjo un error, al cargar los datos</p>';
								})
							}).catch(error => {
								document.getElementById('entidad_list').innerHTML = '<p class="t2_red">Se produjo un error, al cargar los datos</p>';
							})
			    } else if (EntidadSeleccionada == this.config.format_grupo_instalacion_recreativa_acceso_regulacion){
			    		var query = new Query();
							query.where = "1=1";
							query.outFields = ["*"];
							query.orderByFields = ["regulacion_id"];							
							var query_elements = new QueryTask(url_capa);
							consultas.push(query_elements.execute(query));

							Promise.all(consultas).then(results => {
								var all_items = [];
								for (var i = 0; i < results.length; i++) {
									resultados.push(results[i].features);
								}
								for (var i = 0; i < resultados.length; i++) {
									for (var j = 0; j < resultados[i].length; j++) {
										all_items.push(new this.Entidad(resultados[i][j], i));
									}
								}
								this.elementos_eq = all_items;
								var resultados_acceso = [];
								var consultas_acceso = [];
								var query_where_acceso="";
								var valor = document.getElementById('buscador_text').value.toUpperCase();
								var globalid_acceso ="globalid IN (";
					      for (i = 0; i < this.elementos_eq.length; i++) {        
					        globalid_acceso += "'"+this.elementos_eq[i].features.attributes.grupo_instala_recrea_acceso_id+"',";   
					      }
						    globalid_acceso += "'')";
						    if (valor != "") {
							      valor = "%" + valor + "%";
							      query_where_acceso += "( UPPER(nombre) LIKE '" + valor + "') AND ";
							    }
								if (query_where_acceso == ""){
									query_where_acceso = globalid_acceso;
								} else {
									query_where_acceso += globalid_acceso;
								}
								var query_acceso = new Query();
								query_acceso.where = query_where_acceso;
								query_acceso.outFields = ["*"];							
								var query_elements_acceso = new QueryTask(this.url_acceso);
								consultas_acceso.push(query_elements_acceso.execute(query_acceso));
								Promise.all(consultas_acceso).then(results => {
									var all_items_acceso = [];
									for (var i = 0; i < results.length; i++) {
										resultados_acceso.push(results[i].features);
									}
									for (var i = 0; i < resultados_acceso.length; i++) {
										for (var j = 0; j < resultados_acceso[i].length; j++) {
											all_items_acceso.push(new this.Entidad(resultados_acceso[i][j], i));
										}
									}
									this.elementos_aux = all_items_acceso;
									this.load_table_buscador(this.config.format_grupo_instalacion_recreativa_acceso_regulacion);
								}).catch(error => {
									document.getElementById('entidad_list').innerHTML = '<p class="t2_red">Se produjo un error, al cargar los datos</p>';
								})
							}).catch(error => {
								document.getElementById('entidad_list').innerHTML = '<p class="t2_red">Se produjo un error, al cargar los datos</p>';
							})			    
			    } else {
			    	document.getElementById('entidad_list').innerHTML = '<p class="t2_red">Se produjo un error, al cargar los datos</p>';
			    }
			  }				
			},
			
			load_table_buscador: function (tipo) {
				var htmlToAdd = '';
				if (this.elementos_eq.length == 0) {
					document.getElementById('no_list').style.display = 'block';
				}
				else {			
					document.getElementById('entidad_list').style.display = 'block';		
					htmlToAdd += '<ul class="list-group">';
					for (var i = 0; i < this.elementos_eq.length; i++) {
						if (tipo == "CodigoNombre") {
							htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_eq[i].features.attributes.codigo + " | " + this.elementos_eq[i].features.attributes.nombre + "</span></li>";
						} else if (tipo == this.config.format_red_recreativa_regulacion) {
							if (this.elementos_aux.length == 0) {
								document.getElementById('entidad_list').style.display = 'none';
								document.getElementById('no_list').style.display = 'block';
							} else {
								for (var j = 0; j < this.elementos_aux.length; j++){
									if (this.elementos_eq[i].features.attributes.red_recreativa_id == this.elementos_aux[j].features.attributes.globalid) {					
										htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_aux[j].features.attributes.matricula + " | " + this.elementos_aux[j].features.attributes.nombre + " | " + this.DomRegulacion[this.elementos_eq[i].features.attributes.regulacion_id] + "</span></li>";
						    	}
						    }
						  }
						}	else if (tipo == this.config.format_grupo_instalacion_recreativa_acceso_regulacion) {							
							if (this.elementos_aux.length == 0) {
								document.getElementById('entidad_list').style.display = 'none';
								document.getElementById('no_list').style.display = 'block';
							} else {
								for (var j = 0; j < this.elementos_aux.length; j++){
									if (this.elementos_eq[i].features.attributes.grupo_instala_recrea_acceso_id == this.elementos_aux[j].features.attributes.globalid) {					
										htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_aux[j].features.attributes.nombre + " | " + this.DomRegulacion[this.elementos_eq[i].features.attributes.regulacion_id] + "</span></li>";
						    	}
						    }
						  }						
						} else {
							htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_eq[i].features.attributes.globalid + "</span></li>";
						}
					}
					htmlToAdd += '</ul>';
				}
				document.getElementById('entidad_list').innerHTML += htmlToAdd;
			},

			load_table_selected: function (tipo) {
				this.tablaPos_fin = [];
				document.getElementById('entidad_list_select').innerHTML = '';				
				if (this.selected_eq.length == 0) {
					document.getElementById('no_list').style.display = 'block';
				}
				else {
					var ul=DomConstruct.toDom("<ul class='list-group'>");
				  DomConstruct.place(ul,'entidad_list_select','last');
					for (var i = 0; i < this.selected_eq.length; i++) {
						if (tipo == "CodigoNombre") {
							var li=DomConstruct.toDom("<li class='list-group-item'>");
						  var input=DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_select_" + i + "'>");
						  on(input,'click',lang.hitch(this, this.comprobar_graphics(this.selected_eq[i].features.attributes.globalid,i,"check_btn_select_"))); 
						  var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.selected_eq[i].features.attributes.codigo + " | " + this.selected_eq[i].features.attributes.nombre + "</span>");
						  on(span,'click',lang.hitch(this, this.resaltar_graphics(this.selected_eq[i].features.attributes.globalid,i,"check_btn_select_")));
						  DomConstruct.place(input,li,'last');
						  DomConstruct.place(span,li,'last');
						  DomConstruct.place(li,ul,'last');
						  this.tablaPos_fin[i] = new this.Tabla_localizador(i,this.selected_eq[i].features.attributes.globalid);
						} else if (tipo == this.config.format_red_recreativa_regulacion) {	
							for (var j = 0; j < this.elementos_aux.length; j++){
								if (this.selected_eq[i].features.attributes.red_recreativa_id == this.elementos_aux[j].features.attributes.globalid) {	
									var li=DomConstruct.toDom("<li class='list-group-item'>");
								  var input=DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_select_" + i + "'>");
								  var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_aux[j].features.attributes.matricula + " | " + this.elementos_aux[j].features.attributes.nombre + " | " +this.DomRegulacion[this.selected_eq[i].features.attributes.regulacion_id] + "</span>");
								  DomConstruct.place(input,li,'last');
								  DomConstruct.place(span,li,'last');
								  DomConstruct.place(li,ul,'last');										  	
								}
						  }					
						}	else if (tipo == this.config.format_grupo_instalacion_recreativa_acceso_regulacion) {
							for (var j = 0; j < this.elementos_aux.length; j++){
									if (this.selected_eq[i].features.attributes.grupo_instala_recrea_acceso_id == this.elementos_aux[j].features.attributes.globalid) {					
										var li=DomConstruct.toDom("<li class='list-group-item'>");
									  var input=DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_select_" + i + "'>");
									  var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_aux[j].features.attributes.nombre + " | " + this.DomRegulacion[this.selected_eq[i].features.attributes.regulacion_id] + "</span>");
									  DomConstruct.place(input,li,'last');
									  DomConstruct.place(span,li,'last');
									  DomConstruct.place(li,ul,'last');
									}
						  }
						} else {
							var li=DomConstruct.toDom("<li class='list-group-item'>");
						  var input=DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_select_" + i + "'>");
						  var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.selected_eq[i].features.attributes.globalid + "</span>");
						  DomConstruct.place(input,li,'last');
						  DomConstruct.place(span,li,'last');
						  DomConstruct.place(li,ul,'last');
						}
					}
					this.tablaPos_base = this.tablaPos_fin;				
				}				
			},
			
			resaltar_graphics: function (globalid,id_button,tipo) {				
					return function () {
						var id = tipo + id_button;
						if (document.getElementById(id) != null && document.getElementById(id).checked == true) {
							for (var i = 0; i < this.Graphics_eqd.graphics.length; i++) {
						  	if (globalid == this.Graphics_eqd.graphics[i].attributes.globalid) {
									if (this.Graphics_eqd.graphics[i].geometry.__proto__.declaredClass == 'esri.geometry.Polyline'){
								  		var line = new SimpleLineSymbol();
								  		line.setWidth(5);
											line.setColor(new Color([230, 0, 0, 1]));											
									  	this.Graphics_eqd.graphics[i].setSymbol(line);										
								  	} else {
									  	var line = new SimpleLineSymbol();
											line.setColor(new Color([230, 0, 0, 1]));
											var marker = new SimpleMarkerSymbol();
											marker.setOutline(line);
											marker.setColor(new Color([230, 0, 0, 0.25]));
											marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
											this.Graphics_eqd.graphics[i].setSymbol(marker);
									  }
						  	} else {
						  		for(var j = 0; j < this.tablaPos_base.length; j++){
						  			 var id = tipo + this.tablaPos_base[j].posicion;
						  		   if (document.getElementById(id).checked == true && this.tablaPos_base[j].globalid == this.Graphics_eqd.graphics[i].attributes.globalid)
											if (this.Graphics_eqd.graphics[i].geometry.__proto__.declaredClass == 'esri.geometry.Polyline'){
										  		var line = new SimpleLineSymbol();
										  		line.setWidth(5);
													line.setColor(new Color([0, 197, 255, 1]));								
											  	this.Graphics_eqd.graphics[i].setSymbol(line);
										  	} else {
											  	var line = new SimpleLineSymbol();
													line.setColor(new Color([0, 197, 255, 1]));
													var marker = new SimpleMarkerSymbol();
													marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
													marker.setColor(new Color([0, 197, 255, 0.26]));								
													marker.setOutline(line);						  	
											  	this.Graphics_eqd.graphics[i].setSymbol(marker);
											  }
										 }
						  		}
						  	}						  		
						 }
					}				
			},
			
			comprobar_graphics: function (globalid,id_button,tipo) {
				
					return function () {
						var id = tipo + id_button;
						if (document.getElementById(id) != null && document.getElementById(id).checked == true) {
								for (var i = 0; i < this.Graphics_eqd.graphics.length; i++) {
									if (globalid == this.Graphics_eqd.graphics[i].attributes.globalid){
										if (this.Graphics_eqd.graphics[i].geometry.__proto__.declaredClass == 'esri.geometry.Polyline'){
											var line = new SimpleLineSymbol();
											line.setWidth(5);
											line.setColor(new Color([0, 197, 255, 1]));								
											this.Graphics_eqd.graphics[i].setSymbol(line);
								  	} else {
									  	var line = new SimpleLineSymbol();
											line.setColor(new Color([0, 197, 255, 1]));
											var marker = new SimpleMarkerSymbol();
											marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
											marker.setColor(new Color([0, 197, 255, 0.26]));								
											marker.setOutline(line);						  	
									  	this.Graphics_eqd.graphics[i].setSymbol(marker);
									  }		
									} 
								}
					  } else {
					  		for (var i = 0; i < this.Graphics_eqd.graphics.length; i++) {
									if (globalid == this.Graphics_eqd.graphics[i].attributes.globalid){										
										this.Graphics_eqd.graphics[i].setSymbol(null);
									}
								}					  	
					  }							
					}				
			},

			botonDraw: function () {
				document.getElementById('geometry_oblig').style.display = 'none';
				this.Graphics_eqd.clear();
				this.draw.activate('freehandpolygon');
			},
			
			geoEquipamiento: function (event) {
				this.draw.deactivate();
				this.geometry_eq = event.geometry;
				document.getElementById('geometry_oblig').style.display = 'none';
				document.getElementById('select_region').style.display = 'none';
				document.getElementById('least_one_list').style.display = 'none';	
				document.getElementById('layer_novisible').style.display = 'none';	
				document.getElementById('entidad_list').innerHTML = '';				
				document.getElementById('select_polygon').innerHTML = "Cargando...";
				//consultar elementos relacionados pillar dato del grupo
				var EntidadSeleccionada = document.getElementById("selec_entidad").value;
				this.entidadEq = EntidadSeleccionada;
				var resultados = [];
				var consultas = [];				
				this.elementos_eq = [];
				var url_capa = "";
				var id_capa = "";
				var pos_entidad = 0;
				for (var i = 0; i < this.layerEntidades.length; i++) {
			  	if (this.layerEntidades[i].related_layers_title == EntidadSeleccionada) {
			  		url_capa=this.layerEntidades[i].related_layers_url;
			  		id_capa=this.layerEntidades[i].related_layers_id;
			  		pos_entidad=i;
			  		break;
			  	}
			  }
			  this.url_eq = url_capa;
			  this.id_eq = id_capa;
			  this.url_relation = this.layerRelations[pos_entidad].related_layers_url;
			  
			  if (this.map._layers[this.layerEntidades[pos_entidad].related_layers_id].visible == true) {
						var query = new Query();
						query.geometry = this.geometry_eq;
						query.where = "1=1";
						query.outFields = ["*"];
						query.returnGeometry = true;																	
						var query_elements = new QueryTask(url_capa);
						consultas.push(query_elements.execute(query));
						
						Promise.all(consultas).then(results => {
							var all_items_eq = [];
							document.getElementById('select_polygon').innerHTML = "Seleccionar en el Mapa";
							for (var i = 0; i < results.length; i++) {
								resultados.push(results[i].features);
							}
							for (var i = 0; i < resultados.length; i++) {
								for (var j = 0; j < resultados[i].length; j++) {
									all_items_eq.push(new this.Entidad(resultados[i][j], i));
								}
							}
							this.elementos_eq = all_items_eq;								
		    			var globalid_selected ="globalid IN (";
				      for (i = 0; i < this.elementos_eq.length; i++) {        
				        globalid_selected += "'"+this.elementos_eq[i].features.attributes.globalid+"',"; 
				      }
					    globalid_selected += "'')";
					    var query_g = new Query();							
							query_g.where = globalid_selected;
							query_g.outFields = ["*"];
							query_g.returnGeometry = true;
							this.map._layers[this.id_eq].selectFeatures(query_g,FeatureLayer.SELECTION_NEW,(result_select_graphics) => {
								this.Graphics_eqd.clear();
								for (var i = 0; i < result_select_graphics.length; i++) {
									if (this.config.capas_lineales.includes(this.entidadEq)){
										var line = new SimpleLineSymbol();
										line.setWidth(5);
										line.setColor(new Color([0, 197, 255, 1]));
										result_select_graphics[i].symbol = line;
										this.Graphics_eqd.add(result_select_graphics[i]);
										if(i == result_select_graphics.length - 1){
											this.Graphics_eqd.clear();
											for (var i = 0; i < result_select_graphics.length; i++) {
												this.Graphics_eqd.add(result_select_graphics[i]);
											}
										}							  								  	
									} else {
										var line = new SimpleLineSymbol();
											line.setColor(new Color([0, 197, 255, 1]));
											var marker = new SimpleMarkerSymbol();
											marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
											marker.setColor(new Color([0, 197, 255, 0.26]));
											marker.setAngle(0);
											marker.setOutline(line);
										result_select_graphics[i].symbol = marker;
										this.Graphics_eqd.add(result_select_graphics[i]);
									}
							  }			
								this.Graphics_eqd.redraw();																    	
							}
							,(rollback_select_graphics) => {
										this.Graphics_eqd.clear();
							});	
							this.load_table();
						}).catch(error => {
							this.Graphics_eqd.clear();
							document.getElementById('select_polygon').innerHTML = "Seleccionar en el Mapa";
							document.getElementById('entidad_list').innerHTML = '<p class="t2_red">Se produjo un error, selecciona la geometría nuevamente</p>';
						})
				} else {
					document.getElementById('select_polygon').innerHTML = "Seleccionar en el Mapa";
					document.getElementById('layer_novisible').style.display = 'block';
				}				
			},
			
			load_table: function () {
				this.tablaPos_ini =	[];	
				if (this.elementos_eq.length == 0) {
					document.getElementById('select_region').style.display = 'block';
				}
				else {
					document.getElementById('entidad_list').style.display = 'block';
					var ul=DomConstruct.toDom("<ul class='list-group'>");
				  DomConstruct.place(ul,'entidad_list','last');							
					for (var i = 0; i < this.elementos_eq.length; i++) {
						if (this.elementos_eq[i].features.attributes.codigo != undefined && this.elementos_eq[i].features.attributes.nombre != undefined) {
							var li=DomConstruct.toDom("<li class='list-group-item'>");
						  var input=DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_" + i + "'>");
						  on(input,'click',lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid,i,"check_btn_"))); 
						  var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_eq[i].features.attributes.codigo + " | " + this.elementos_eq[i].features.attributes.nombre + "</span>");
						  on(span,'click',lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid,i,"check_btn_")));
						  DomConstruct.place(input,li,'last');
						  DomConstruct.place(span,li,'last');
						  DomConstruct.place(li,ul,'last');
						  this.tablaPos_ini[i] = new this.Tabla_localizador(i,this.elementos_eq[i].features.attributes.globalid);
						}
						else {
							if (this.elementos_eq[i].features.attributes.source != undefined) {
								var li=DomConstruct.toDom("<li class='list-group-item'>");
							  var input=DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_" + i + "'>");
							  on(input,'click',lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid,i,"check_btn_"))); 
							  var span = DomConstruct.toDom("<span class='t2_tab'>" + " Tramo: " + this.elementos_eq[i].features.attributes.globalid + " | " + this.elementos_eq[i].features.attributes.nombre + "</span>");
							  on(span,'click',lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid,i,"check_btn_")));
							  DomConstruct.place(input,li,'last');
							  DomConstruct.place(span,li,'last');
							  DomConstruct.place(li,ul,'last');
							  this.tablaPos_ini[i] = new this.Tabla_localizador(i,this.elementos_eq[i].features.attributes.globalid);
						  }
							else {
								var li=DomConstruct.toDom("<li class='list-group-item'>");
							  var input=DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_" + i + "'>");
							  on(input,'click',lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid,i,"check_btn_"))); 
							  var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_eq[i].features.attributes.globalid + "</span>");
							  on(span,'click',lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid,i,"check_btn_")));
							  DomConstruct.place(input,li,'last');
							  DomConstruct.place(span,li,'last');
							  DomConstruct.place(li,ul,'last');
							  this.tablaPos_ini[i] = new this.Tabla_localizador(i,this.elementos_eq[i].features.attributes.globalid);
						  }
						}
					}
					this.tablaPos_base =	this.tablaPos_ini;				
				}				
			},
			
			create_new_relacion: function () {
				document.getElementById('loading_msg').innerHTML = "Guardando el documento relacionado con las entidades seleccionadas...";
				document.getElementById('loading_msg').className = 't2';
				var tabla_documento = new FeatureLayer(this.url_doc);
				var nuevoDocumento = [];
				nuevoDocumento[0] = new Graphic();
				nuevoDocumento[0].attributes = {
					id: null, codigo: null, nombre: null, descripcion: null, idioma_id: null, uso_interno: null, editorial: null, 
					fecha_revision: null, visible: null, codigo_anterior: null, fecha_modificacion: null, uuid: null, nombre_fichero_anterior: null, 
					documento_id: null, url: null, etiqueta: null
				};		
				nuevoDocumento[0].attributes.id = 0;		
				nuevoDocumento[0].attributes.nombre = this.doc_nombre;
				nuevoDocumento[0].attributes.descripcion = this.doc_descricion;
				nuevoDocumento[0].attributes.idioma_id = this.doc_idioma;
				nuevoDocumento[0].attributes.uso_interno = this.doc_uso_interno;
				nuevoDocumento[0].attributes.editorial = this.doc_editorial;
				nuevoDocumento[0].attributes.visible = this.doc_visible;
				nuevoDocumento[0].attributes.codigo_anterior = this.doc_codigo_anterior;
				nuevoDocumento[0].attributes.fecha_modificacion=Date.now();
				nuevoDocumento[0].attributes.nombre_fichero_anterior = this.doc_nombre_anterior;
				if (this.entidadEq == this.config.nombre_area_descanso){
					nuevoDocumento[0].attributes.etiqueta="Area descanso";
				} else if (this.entidadEq == this.config.nombre_arqueta){
					nuevoDocumento[0].attributes.etiqueta="Arqueta";
				}	else if (this.entidadEq == this.config.nombre_barrera){
					nuevoDocumento[0].attributes.etiqueta="Barrera";
				} else if (this.entidadEq == this.config.nombre_cartel){
					nuevoDocumento[0].attributes.etiqueta="Cartel";
				} else if (this.entidadEq == this.config.nombre_deposito){
					nuevoDocumento[0].attributes.etiqueta="Deposito";
				} else if (this.entidadEq == this.config.nombre_dotacion_s){
					nuevoDocumento[0].attributes.etiqueta="Dotacion de sendero";
				} else if (this.entidadEq == this.config.nombre_instalacion_recreativa){
					nuevoDocumento[0].attributes.etiqueta="Instalacion recreativa";
				} else if (this.entidadEq == this.config.nombre_mesa){
					nuevoDocumento[0].attributes.etiqueta="Mesa";
				} else if (this.entidadEq == this.config.nombre_panel){
					nuevoDocumento[0].attributes.etiqueta="Panel";
				} else if (this.entidadEq == this.config.nombre_red_recreativa_regulacion){
					nuevoDocumento[0].attributes.etiqueta="Red recreativa regulacion";
				} else if (this.entidadEq == this.config.nombre_grupo_instalacion_recreativa_acceso_regulacion){
					nuevoDocumento[0].attributes.etiqueta="Grupo instalacion recreativa acceso regulacion";				
				} else if (this.entidadEq == this.config.nombre_red_recreativa_sistema_senalizacion){
					nuevoDocumento[0].attributes.etiqueta="Red recreativa sistema senalizacion";
				} else if (this.entidadEq == this.config.nombre_zona_aparcamiento){
					nuevoDocumento[0].attributes.etiqueta="Zona aparcamiento";
				} else {
					nuevoDocumento[0].attributes.etiqueta="";
				}				
				
				tabla_documento.applyEdits(nuevoDocumento, null, null, (applyEditsResultDoc) => {
					var globaliddoc = applyEditsResultDoc[0].globalId;		
					this.doc_objectid = applyEditsResultDoc[0].objectId;
          var arrayRelacionados = [];
          var nuevaRelacion = [];
          var tabla_relacionada = new FeatureLayer(this.url_relation);					
					for (var i = 0; i < this.selected_eq_f.length; i++) {
						nuevaRelacion[i] = new Graphic();
						if (this.entidadEq == this.config.nombre_area_descanso){
							nuevaRelacion[i].attributes = {
								area_descanso_id: null, documento_id: null
							};
							nuevaRelacion[i].attributes.documento_id = globaliddoc;
							nuevaRelacion[i].attributes.area_descanso_id = this.selected_eq_f[i].features.attributes.globalid;
							arrayRelacionados.push(nuevaRelacion[i]);							
						} else if (this.entidadEq == this.config.nombre_arqueta){
							nuevaRelacion[i].attributes = {
								arqueta_id: null, documento_id: null
							};
							nuevaRelacion[i].attributes.documento_id = globaliddoc;
							nuevaRelacion[i].attributes.arqueta_id = this.selected_eq_f[i].features.attributes.globalid;
							arrayRelacionados.push(nuevaRelacion[i]);
						}	else if (this.entidadEq == this.config.nombre_barrera){
							nuevaRelacion[i].attributes = {
								barrera_id: null, documento_id: null
							};
							nuevaRelacion[i].attributes.documento_id = globaliddoc;
							nuevaRelacion[i].attributes.barrera_id = this.selected_eq_f[i].features.attributes.globalid;
							arrayRelacionados.push(nuevaRelacion[i]);
						} else if (this.entidadEq == this.config.nombre_cartel){
							nuevaRelacion[i].attributes = {
								cartel_id: null, documento_id: null
							};
							nuevaRelacion[i].attributes.documento_id = globaliddoc;
							nuevaRelacion[i].attributes.cartel_id = this.selected_eq_f[i].features.attributes.globalid;
							arrayRelacionados.push(nuevaRelacion[i]);
						} else if (this.entidadEq == this.config.nombre_deposito){
							nuevaRelacion[i].attributes = {
								deposito_id: null, documento_id: null
							};
							nuevaRelacion[i].attributes.documento_id = globaliddoc;
							nuevaRelacion[i].attributes.deposito_id = this.selected_eq_f[i].features.attributes.globalid;
							arrayRelacionados.push(nuevaRelacion[i]);
						} else if (this.entidadEq == this.config.nombre_dotacion_s){
							nuevaRelacion[i].attributes = {
								dotacion_p_id: null, documento_id: null
							};
							nuevaRelacion[i].attributes.documento_id = globaliddoc;
							nuevaRelacion[i].attributes.dotacion_p_id = this.selected_eq_f[i].features.attributes.globalid;
							arrayRelacionados.push(nuevaRelacion[i]);						
						} else if (this.entidadEq == this.config.nombre_instalacion_recreativa){
							nuevaRelacion[i].attributes = {
								instalacion_recreativa_id: null, documento_id: null
							};
							nuevaRelacion[i].attributes.documento_id = globaliddoc;
							nuevaRelacion[i].attributes.instalacion_recreativa_id = this.selected_eq_f[i].features.attributes.globalid;
							arrayRelacionados.push(nuevaRelacion[i]);
						} else if (this.entidadEq == this.config.nombre_mesa){
							nuevaRelacion[i].attributes = {
								mesa_id: null, documento_id: null
							};
							nuevaRelacion[i].attributes.documento_id = globaliddoc;
							nuevaRelacion[i].attributes.mesa_id = this.selected_eq_f[i].features.attributes.globalid;
							arrayRelacionados.push(nuevaRelacion[i]);
						} else if (this.entidadEq == this.config.nombre_panel){
							nuevaRelacion[i].attributes = {
								panel_id: null, documento_id: null
							};
							nuevaRelacion[i].attributes.documento_id = globaliddoc;
							nuevaRelacion[i].attributes.panel_id = this.selected_eq_f[i].features.attributes.globalid;
							arrayRelacionados.push(nuevaRelacion[i]);
						} else if (this.entidadEq == this.config.nombre_red_recreativa_regulacion){
							nuevaRelacion[i].attributes = {
								red_recreativa_regulacion_id: null, documento_id: null
							};
							nuevaRelacion[i].attributes.documento_id = globaliddoc;
							nuevaRelacion[i].attributes.red_recreativa_regulacion_id = this.selected_eq_f[i].features.attributes.globalid;
							arrayRelacionados.push(nuevaRelacion[i]);
						} else if (this.entidadEq == this.config.nombre_grupo_instalacion_recreativa_acceso_regulacion){
							nuevaRelacion[i].attributes = {
								grupo_insta_recre_acc_regu_id: null, documento_id: null
							};
							nuevaRelacion[i].attributes.documento_id = globaliddoc;
							nuevaRelacion[i].attributes.grupo_insta_recre_acc_regu_id = this.selected_eq_f[i].features.attributes.globalid;
							arrayRelacionados.push(nuevaRelacion[i]);
						} else if (this.entidadEq == this.config.nombre_red_recreativa_sistema_senalizacion){
							nuevaRelacion[i].attributes = {
								red_recrea_sist_senalizacion_id: null, documento_id: null
							};
							nuevaRelacion[i].attributes.documento_id = globaliddoc;
							nuevaRelacion[i].attributes.red_recrea_sist_senalizacion_id = this.selected_eq_f[i].features.attributes.globalid;
							arrayRelacionados.push(nuevaRelacion[i]);
						} else if (this.entidadEq == this.config.nombre_zona_aparcamiento){
							nuevaRelacion[i].attributes = {
								zona_aparcamiento_id: null, documento_id: null
							};
							nuevaRelacion[i].attributes.documento_id = globaliddoc;
							nuevaRelacion[i].attributes.zona_aparcamiento_id = this.selected_eq_f[i].features.attributes.globalid;
							arrayRelacionados.push(nuevaRelacion[i]);
						} else {
							
						}
					}
					tabla_relacionada.applyEdits(arrayRelacionados, null, null, (applyEditsResultRel) => {
						var form = document.getElementById("myForm");
						var tabla_documento_att = new FeatureLayer(this.url_doc);
						tabla_documento_att.addAttachment(this.doc_objectid, form).then(function (result) {
                  document.getElementById('loading_msg').innerHTML = 'Documento y relaciones, almacenadas correctamente.';
			            document.getElementById('loading_msg').className = 't2';
									document.getElementById('btn_at_fi_4').style.display = 'none';
									document.getElementById('end').style.display = 'block';	
            }).catch(function (err) {
                  document.getElementById('loading_msg').innerHTML = 'Se produjo un error al adjuntar el documento, inténtelo de nuevo';
            			document.getElementById('loading_msg').className = 't2_red';
            			document.getElementById('btn_at_fi_4').style.display = 'block';
            });           			
          }, (applyEditsErr) => {
            document.getElementById('loading_msg').innerHTML = 'Se produjo un error al guardar los datos de Tablas Relacionadas, inténtelo de nuevo';
            document.getElementById('loading_msg').className = 't2_red';
            document.getElementById('btn_at_fi_4').style.display = 'block';
          })
				}, (applyEditsErr) => {
					document.getElementById('loading_msg').innerHTML = 'Se produjo un error al guardar los datos del Documento, inténntelo de nuevo';
					document.getElementById('loading_msg').className = 't2_red';
					document.getElementById('btn_at_fi_4').style.display = 'block';
				})				
			},
			
			go_inicio: function () {
				this.reseteo();
				this.load_page(0);
			},
			
			reseteo: function () {				
				this.elementos_eq = [];			
				this.geometry_eq = '';
				this.Graphics_eqd.clear();				
				document.getElementById('selec_entidad').value = "";
				document.getElementById('select_polygon').style.display = 'none';
		    document.getElementById('buscador').style.display = 'none';
				document.getElementById('option1').checked = '';
		    document.getElementById('option2').checked = '';
		    document.getElementById('option1').disabled = false;
				document.getElementById('entidad_list').innerHTML = '';							
				document.getElementById('end').style.display = 'none';
				document.getElementById('btn_at_fi_4').style.display = 'flex';
				document.getElementById('loading_msg').innerHTML = '';
				document.getElementById('nombre_doc').value = '';
				document.getElementById('descripcion_doc').value = '';
				document.getElementById('idioma_doc').value = '38';
				document.getElementById('uso_interno_doc').value = '0';
				document.getElementById('editorial_doc').value = '';
				document.getElementById('visible_doc').value = '0';
				document.getElementById('codigo_anterior_doc').value = '';				
				document.getElementById('nombre_anterior_doc').value = '';
				this._uploadField.value = '';
			},
			
			load_page: function (num) {
				document.getElementById('inicio').style.display = 'none';
				document.getElementById('form_1').style.display = 'none';
				document.getElementById('form_2').style.display = 'none';
				document.getElementById('form_3').style.display = 'none';
				document.getElementById('form_4').style.display = 'none';
				document.getElementById('foot_1').className = 'dot';
				document.getElementById('foot_2').className = 'dot';
				document.getElementById('foot_3').className = 'dot';
				document.getElementById('foot_4').className = 'dot';
				if (num > 0 && num <= 4) {
					document.getElementById('footer').style.display = 'block';
				}
				else {
					document.getElementById('footer').style.display = 'none';
					if (num == 0) { document.getElementById('inicio').style.display = 'block'; }
				}
				switch (num) {
					case 1:
						document.getElementById('foot_1').className = 'dot_check';
						document.getElementById('form_1').style.display = 'block';
						break;
					case 2:
						document.getElementById('foot_2').className = 'dot_check';
						document.getElementById('form_2').style.display = 'block';
						break;
					case 3:
						document.getElementById('foot_3').className = 'dot_check';
						document.getElementById('form_3').style.display = 'block';
						break;
					case 4:
						document.getElementById('foot_4').className = 'dot_check';
						document.getElementById('form_4').style.display = 'block';
						break;
				}
			},
			
			Relations: function (related_layers_url, related_layers_id, related_layers_title) {
				this.related_layers_url = related_layers_url;
				this.related_layers_id = related_layers_id;
				this.related_layers_title = related_layers_title;
			},
			
			Tabla_localizador: function (posicion, globalid) {
				this.posicion = posicion;
				this.globalid = globalid;
			},
			
			Entidad: function (features, index) {
				this.features = features;
				this.index = index;
			}
			
		});
	});


