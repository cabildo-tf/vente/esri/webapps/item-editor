define(['jimu/dijit/FeaturelayerChooserFromMap', "esri/geometry/Point", "esri/SpatialReference", 'jimu/dijit/LoadingShelter', "dojo/dom-class", 'dojo/on', "esri/map",
	'jimu/dijit/LayerChooserFromMapWithDropbox', 'dojo/_base/declare', "esri/dijit/LayerList", "esri/arcgis/utils", 'jimu/BaseWidget', 'jimu/BaseWidgetSetting', 'dojo/_base/lang', 'jimu/LayerInfos/LayerInfos', 'dojo/_base/array', "dojo/dom-attr",
	"esri/toolbars/draw", 'esri/layers/GraphicsLayer', "esri/layers/FeatureLayer", "esri/tasks/QueryTask", "esri/tasks/query", "esri/graphic",
	'esri/geometry/Point', 'esri/geometry/Polyline', 'esri/symbols/SimpleLineSymbol', 'esri/symbols/SimpleFillSymbol', 'esri/symbols/SimpleMarkerSymbol', 'esri/Color', "esri/tasks/GeometryService", "esri/tasks/BufferParameters", 'esri/dijit/editing/AttachmentEditor', "dojo/parser", "esri/geometry/Extent", "dojo/dom-construct", "dojo/domReady!"],
	function (FeaturelayerChooserFromMap, Point, SpatialReference, LoadingShelter, dojoClass, on, Map,
		LayerChooserFromMapWithDropbox, declare, LayerList, arcgisUtils, BaseWidget, BaseWidgetSetting, lang, LayerInfos, array, domAttr, Draw, GraphicsLayer, FeatureLayer, QueryTask, Query, Graphic, Point, Polyline, SimpleLineSymbol, SimpleFillSymbol, SimpleMarkerSymbol, Color, GeometryService, BufferParameters, AttachmentEditor, Parser, Extent, DomConstruct) {
		return declare([BaseWidget], {
			baseClass: 'jimu-widget-customwidget',
			draw: null,
			Graphics_eqr: null,
			RelacionSeleccionada: "",
			url_red_recreativa: "",
			url_documento: "",
			url_red_hidraulica: "",
			url_barrera: "",
			url_relacion: "",
			url_acceso: "",			
			selected_relacion: [],
			selected_relacion_f: [],
			elementos_relacion: [],
			cont: 0,
			layerEntidades: [],
			layerRelations: [],
			layerEntidadesD: [],
			layerRelationsD: [],
			layerGeometriaD: [],
			layerTablasD: [],
			layerEntidadesRH: [],
			layerRelationsRH: [],
			layerEntidadesRR: [],
			layerRelationsRR: [],
			layerEntidadesB: [],
			layerRelationsB: [],
			geometry_eq: "",
			layerEquipamientos: [],
			elementos_eq: [],
			elementos_aux: [],
			selected_eq: [],
			selected_eq_f: [],
			graphics_globalid: [],
			tablaPos_base: [],
			tablaPos_ini: [],
			tablaPos_fin: [],
			valores_seleccionados_Territorio: ")",
			Territorio: [],
			TerritorioValue: [],
			DomRegulacion: [],
			DomLimitacion: [],
			marcar_todos: false,

			onOpen: function () {

			},
			onClose: function () {
				this.draw.deactivate();
				if (document.getElementById('end_nm').style.display == 'block') {
					document.getElementById('dialogo_exit_nm').style.display = 'none';
					this.go_inicio();
				} else if (document.getElementById('inicio_nm').style.display == 'block') {
					document.getElementById('dialogo_exit_nm').style.display = 'none';
				} else {
					document.getElementById('dialogo_exit_nm').style.display = 'block';
				}
			},
			startup: function () {
				gsvc = new GeometryService(this.config.GeometryServer);
				this.Graphics_eqr = new GraphicsLayer({ "id": 'glEquipamientos' });
				this.map.addLayer(this.Graphics_eqr);
				Parser.parse();
				this.inherited(arguments);
				var TipoRelacion = [];
				var NombreEntidadesD = [];
				var NombreRelationsD = [];
				var NombreEntidadesRR = [];
				var NombreRelationsRR = [];
				var NombreEntidadesRH = [];
				var NombreRelationsRH = [];
				var NombreEntidadesB = [];
				var NombreRelationsB = [];

				//Dominio Regulacion				
				//Obtener los keys de las entidades del Config				
				for (const [key, value] of Object.entries(this.config.Regulacion)) {
					this.DomRegulacion[value] = key;;
				}

				//Dominio Limitacion				
				//Obtener los keys de las entidades del Config				
				for (const [key, value] of Object.entries(this.config.Limitacion)) {
					this.DomLimitacion[value] = key;;
				}

				//Obtener los keys de las entidades del Config Documentos
				for (const [key, value] of Object.entries(this.config.RelDocumentos)) {
					NombreEntidadesD.push(key);
					NombreRelationsD.push(value);
				}

				//Entidades relacionadas con documentos
				this.cont = 0;
				for (i = 0; i < NombreEntidadesD.length; i++) {
					for (j = 0; j < this.map.itemInfo.itemData.operationalLayers.length; j++) {
						if (this.map.itemInfo.itemData.operationalLayers[j].title == NombreEntidadesD[i]) {
							this.layerEntidadesD[this.cont] = new this.Relations(this.map.itemInfo.itemData.operationalLayers[j].url, this.map.itemInfo.itemData.operationalLayers[j].id, this.map.itemInfo.itemData.operationalLayers[j].title);
							this.layerGeometriaD.push(this.map.itemInfo.itemData.operationalLayers[j].title);
							this.cont++;
						}
					}
					for (j = 0; j < this.map.itemInfo.itemData.tables.length; j++) {
						if (this.map.itemInfo.itemData.tables[j].title == NombreEntidadesD[i]) {
							this.layerEntidadesD[this.cont] = new this.Relations(this.map.itemInfo.itemData.tables[j].url, this.map.itemInfo.itemData.tables[j].id, this.map.itemInfo.itemData.tables[j].title);
							this.layerTablasD.push(this.map.itemInfo.itemData.tables[j].title);
							this.cont++;
						}
					}
				}

				//Tablas N-M relacionados con documentos
				this.cont = 0;
				for (i = 0; i < NombreRelationsD.length; i++) {
					for (j = 0; j < this.map.itemInfo.itemData.tables.length; j++) {
						if (this.map.itemInfo.itemData.tables[j].title == NombreRelationsD[i]) {
							this.layerRelationsD[this.cont] = new this.Relations(this.map.itemInfo.itemData.tables[j].url, this.map.itemInfo.itemData.tables[j].id, this.map.itemInfo.itemData.tables[j].title);
							this.cont++;
						}
					}
				}

				//Obtener los keys de las entidades del Config Red hidráulica		
				for (const [key, value] of Object.entries(this.config.RelRedhidráulica)) {
					NombreEntidadesRH.push(key);
					NombreRelationsRH.push(value);
				}

				//Entidades relacionadas con Red hidráulica
				this.cont = 0;
				for (i = 0; i < NombreEntidadesRH.length; i++) {
					for (j = 0; j < this.map.itemInfo.itemData.operationalLayers.length; j++) {
						if (this.map.itemInfo.itemData.operationalLayers[j].title == NombreEntidadesRH[i]) {
							this.layerEntidadesRH[this.cont] = new this.Relations(this.map.itemInfo.itemData.operationalLayers[j].url, this.map.itemInfo.itemData.operationalLayers[j].id, this.map.itemInfo.itemData.operationalLayers[j].title);
							this.cont++;
						}
					}
				}

				//Tablas N-M relacionados con Red hidráulica
				this.cont = 0;
				for (i = 0; i < NombreRelationsRH.length; i++) {
					for (j = 0; j < this.map.itemInfo.itemData.tables.length; j++) {
						if (this.map.itemInfo.itemData.tables[j].title == NombreRelationsRH[i]) {
							this.layerRelationsRH[this.cont] = new this.Relations(this.map.itemInfo.itemData.tables[j].url, this.map.itemInfo.itemData.tables[j].id, this.map.itemInfo.itemData.tables[j].title);
							this.cont++;
						}
					}
				}
				
				//Obtener los keys de las entidades del Config Barreas		
				for (const [key, value] of Object.entries(this.config.RelBarreras)) {
					NombreEntidadesB.push(key);
					NombreRelationsB.push(value);
				}

				//Entidades relacionadas con Barreras
				this.cont = 0;
				for (i = 0; i < NombreEntidadesB.length; i++) {
					for (j = 0; j < this.map.itemInfo.itemData.operationalLayers.length; j++) {
						if (this.map.itemInfo.itemData.operationalLayers[j].title == NombreEntidadesB[i]) {
							this.layerEntidadesB[this.cont] = new this.Relations(this.map.itemInfo.itemData.operationalLayers[j].url, this.map.itemInfo.itemData.operationalLayers[j].id, this.map.itemInfo.itemData.operationalLayers[j].title);
							this.cont++;
						}
					}
				}

				//Tablas N-M relacionados con Barreras
				this.cont = 0;
				for (i = 0; i < NombreRelationsB.length; i++) {
					for (j = 0; j < this.map.itemInfo.itemData.tables.length; j++) {
						if (this.map.itemInfo.itemData.tables[j].title == NombreRelationsB[i]) {
							this.layerRelationsB[this.cont] = new this.Relations(this.map.itemInfo.itemData.tables[j].url, this.map.itemInfo.itemData.tables[j].id, this.map.itemInfo.itemData.tables[j].title);
							this.cont++;
						}
					}
				}

				//Obtener los keys de las entidades del Config Redes recreativas
				for (const [key, value] of Object.entries(this.config.RelRedesrecreativas)) {
					NombreEntidadesRR.push(key);
					NombreRelationsRR.push(value);
				}

				//Entidades relacionadas con Redes recreativas
				this.cont = 0;
				for (i = 0; i < NombreEntidadesRR.length; i++) {
					for (j = 0; j < this.map.itemInfo.itemData.operationalLayers.length; j++) {
						if (this.map.itemInfo.itemData.operationalLayers[j].title == NombreEntidadesRR[i]) {
							this.layerEntidadesRR[this.cont] = new this.Relations(this.map.itemInfo.itemData.operationalLayers[j].url, this.map.itemInfo.itemData.operationalLayers[j].id, this.map.itemInfo.itemData.operationalLayers[j].title);
							this.cont++;
						}
					}
				}

				//Tablas N-M relacionados con Redes recreativas
				this.cont = 0;
				for (i = 0; i < NombreRelationsRR.length; i++) {
					for (j = 0; j < this.map.itemInfo.itemData.tables.length; j++) {
						if (this.map.itemInfo.itemData.tables[j].title == NombreRelationsRR[i]) {
							this.layerRelationsRR[this.cont] = new this.Relations(this.map.itemInfo.itemData.tables[j].url, this.map.itemInfo.itemData.tables[j].id, this.map.itemInfo.itemData.tables[j].title);
							this.cont++;
						}
					}
				}

				//Llamar al div del tipo relacion
				let divRelacion = document.getElementById("relacion_nm");
				TipoRelacion = this.config.TipoRelacionNM;

				//Rellenar el combo con los keys
				for (i = 0; i < TipoRelacion.length; i++) {
					divRelacion.innerHTML += '<option value="' + TipoRelacion[i] + '">' + TipoRelacion[i] + '</option>';
				}

				//Llamar al div del Territorio
				let divTerritorio = document.getElementById("desplegableTerritorio_nm");
				//Obtener los keys de las entidades del Config				
				for (const [key, value] of Object.entries(this.config.FiltroPorTerritorio)) {
					this.Territorio.push(key);
					this.TerritorioValue.push(value);
				}

				//Crear div MULTISELECT dropdown
				for (i = 0; i < this.Territorio.length; i++) {
					var div1 = DomConstruct.toDom('<div class="item" data-value="' + this.TerritorioValue[i] + '" role="option" tabindex="-1" aria-selected="true" aria-checked="false">');
					var div2 = DomConstruct.toDom('<div id="seleccionable_' + i + '_Territorio" data-value="' + this.TerritorioValue[i] + '"class="checkInput checkbox Territorio" data="' + this.TerritorioValue[i] + '">');
					var div3 = DomConstruct.toDom('<div class="label jimu-ellipsis-Blanks labelRuntime" style="max-width: 284.8px;">' + this.Territorio[i] + '');
					DomConstruct.place(div1, 'desplegableTerritorio_nm', 'last');
					DomConstruct.place(div2, div1, 'last');
					DomConstruct.place(div3, div1, 'last');
				}
				//Addeventlister Territorio MULTISELEC check
				var htmlCheck = document.getElementById("desplegableTerritorio_nm")
				htmlCheck.addEventListener('click', (event) => {
					this._checking(event)
				})

				//Llamar al div del combo
				let divCombo = document.getElementById("selec_entidadd_nm");

				//Rellenar el combo con los keys
				for (i = 0; i < NombreEntidadesD.length; i++) {
					divCombo.innerHTML += '<option value="' + NombreEntidadesD[i] + '">' + NombreEntidadesD[i] + '</option>';
				}

				//URL de Red Hidraulica
				for (j = 0; j < this.map.itemInfo.itemData.operationalLayers.length; j++) {
					if (this.map.itemInfo.itemData.operationalLayers[j].title == this.config.red_hidraulica) {
						this.url_red_hidraulica = this.map.itemInfo.itemData.operationalLayers[j].url;
						break;
					}
				}
				
				//URL de Barreras
				for (j = 0; j < this.map.itemInfo.itemData.operationalLayers.length; j++) {
					if (this.map.itemInfo.itemData.operationalLayers[j].title == this.config.barrera) {
						this.url_barrera = this.map.itemInfo.itemData.operationalLayers[j].url;
						break;
					}
				}

				//URL de Red recreativa
				for (j = 0; j < this.map.itemInfo.itemData.tables.length; j++) {
					if (this.map.itemInfo.itemData.tables[j].title == this.config.red_recreativa) {
						this.url_red_recreativa = this.map.itemInfo.itemData.tables[j].url;
						break;
					}
				}

				//URL de Documento
				for (j = 0; j < this.map.itemInfo.itemData.tables.length; j++) {
					if (this.map.itemInfo.itemData.tables[j].title == this.config.documento) {
						this.url_documento = this.map.itemInfo.itemData.tables[j].url;
						break;
					}
				}

				//URL de Accesos
				for (j = 0; j < this.map.itemInfo.itemData.tables.length; j++) {
					if (this.map.itemInfo.itemData.tables[j].title == this.config.Grupo_instalacion_recreativa_acceso) {
						this.url_acceso = this.map.itemInfo.itemData.tables[j].url;
						break;
					}
				}
				
				//Boton Comenzar
				var button_fc = document.getElementById('comenzar_nm');
				button_fc.addEventListener('click', () => {
					this.load_page(1);
				});

				//Button Combo Tipo Relacion	
				var RelacionNM = document.getElementById("relacion_nm");
				RelacionNM.addEventListener("change", () => {
					this.Graphics_eqr.clear();
					this.RelacionSeleccionada = document.getElementById("relacion_nm").value;
					document.getElementById('no_list_nm').style.display = 'none';
					document.getElementById('one_list_relacion_nm').style.display = 'none';
					document.getElementById('buscador_text_nm').value = "";
					document.getElementById('equipamientos_list_nm').innerHTML = '';
					document.getElementById('equipamientos_listd_nm').innerHTML = '';
					document.getElementById('buscador_textd_nm').value = "";
					document.getElementById('selec_entidadd_nm').value = "";
					document.getElementById('option1_nm').checked = '';
					document.getElementById('option2_nm').checked = '';
					document.getElementById('option1_nm').disabled = false;
					document.getElementById('buscadord_nm').style.display = 'none';
					document.getElementById('select_polygond_nm').style.display = 'none';
					document.getElementById('rlayer_novisible_nm').style.display = 'none';
					document.getElementById('least_one_list_nm').style.display = 'none';
					document.getElementById('select_region_nm').style.display = 'none';
					document.getElementById('layer_nogeometria_nm').style.display = 'none';
					document.getElementById('combo_one_list_nm').style.display = 'none';
					document.getElementById('layer_novisibled_nm').style.display = 'none';
					document.getElementById('geometry_obligd_nm').style.display = 'none';
					document.getElementById('select_regiond_nm').style.display = 'none';
					document.getElementById('least_one_listd_nm').style.display = 'none';
					document.getElementById('no_listd_nm').style.display = 'none';
					//Resetear mutiselect
					//Filtro Territorio
					document.getElementById("Contador_Territorio_nm").innerHTML = 0
					document.getElementById("FiltroTextoTerritorio_nm").value = "";
					this.valores_seleccionados_Territorio = ")";
					document.getElementById("jimu_dijit_Popup_Territorio_nm").style.display = 'none';
					var filtro_texto = document.getElementById("desplegableTerritorio_nm")
					for (i = 1; i < filtro_texto.childNodes.length; i++) {
						filtro_texto.childNodes[i].style.display = "block";
					}
					for (i = 0; i < this.Territorio.length; i++) {
						var seleccionable = document.getElementById("seleccionable_" + i + "_Territorio")
						if (seleccionable.className == "checkInput checkbox checked Territorio") {
							seleccionable.className = "checkInput checkbox Territorio";
						}
					}
					if (this.RelacionSeleccionada != "") {
						document.getElementById('buscador_nm').style.display = 'flex';
						if (this.RelacionSeleccionada == this.config.red_recreativa) {
							document.getElementById('buscador_text_nm').placeholder = "Introduzca matricula o nombre...";
						} else if (this.RelacionSeleccionada == this.config.red_hidraulica) {
							document.getElementById('buscador_text_nm').placeholder = "Introduzca codigo o nombre...";
						} else if (this.RelacionSeleccionada == this.config.barrera) {
							document.getElementById('buscador_text_nm').placeholder = "Introduzca codigo o nombre...";
						} else if (this.RelacionSeleccionada == this.config.documento) {
							document.getElementById('buscador_text_nm').placeholder = "Introduzca codigo o nombre...";
						} else {
							document.getElementById('buscador_text_nm').placeholder = "Introduzca codigo o nombre...";
						}
						this.rellenar_relacion(this.RelacionSeleccionada);
					} else {
						document.getElementById('buscador_nm').style.display = 'none';
						document.getElementById('relacion_list_nm').style.display = 'none';
					}
				});

				//Text Buscador
				var buscador_f1 = document.getElementById('buscador_text_nm');
				buscador_f1.addEventListener('input', () => {
					this.rellenar_relacion(this.RelacionSeleccionada);
				});

				//Boton Siguiente Formulario_1
				var button_f1 = document.getElementById('end_form_1_nm');
				button_f1.addEventListener('click', () => {
					this.selected_relacion = [];
					for (var i = 0; i < this.elementos_relacion.length; i++) {
						var n = i.toString();
						var id = 'check_btn_relacion_' + n;
						if (document.getElementById(id).checked == true) {
							this.selected_relacion.push(this.elementos_relacion[i]);
						}
					}
					if (this.selected_relacion.length == 0) {
						document.getElementById('one_list_relacion_nm').style.display = 'block';
					}
					else {
						document.getElementById('one_list_relacion_nm').style.display = 'none';
						if (this.RelacionSeleccionada == this.config.red_recreativa) {
							this.layerEntidades = this.layerEntidadesRR;
							this.layerRelations = this.layerRelationsRR;
							this.load_page(2);
						}
						if (this.RelacionSeleccionada == this.config.red_hidraulica) {
							this.layerEntidades = this.layerEntidadesRH;
							this.layerRelations = this.layerRelationsRH;
							this.load_page(2);
						}
						if (this.RelacionSeleccionada == this.config.barrera) {
							this.layerEntidades = this.layerEntidadesB;
							this.layerRelations = this.layerRelationsB;
							this.load_page(2);
						}
						if (this.RelacionSeleccionada == this.config.documento) {
							this.layerEntidades = this.layerEntidadesD;
							this.layerRelations = this.layerRelationsD;
							this.load_page(5);
						}
					}
				});

				//Boton Draw
				var draw_btn = document.getElementById('select_polygon_nm');
				draw_btn.onclick = lang.hitch(this, this.botonDraw);


				var draw_btnd = document.getElementById('select_polygond_nm');
				draw_btnd.onclick = lang.hitch(this, this.botonDraw);

				this.draw = new Draw(this.map);
				this.draw.on('draw-complete', lang.hitch(this, this.geoEquipamiento));

				//Boton Siguiente Formulario_2
				var button_f2 = document.getElementById('end_form_23_nm');
				button_f2.addEventListener('click', () => {
					if (this.geometry_eq == '') {
						document.getElementById('geometry_oblig_nm').style.display = 'block';
					}
					else {
						document.getElementById('geometry_oblig_nm').style.display = 'none';
						this.selected_eq = [];
						for (var i = 0; i < this.elementos_eq.length; i++) {
							var n = i.toString();
							var id = 'check_btn_' + n;
							if (document.getElementById(id) != null && document.getElementById(id).checked == true) {
								this.selected_eq.push(this.elementos_eq[i]);
							}
						}
						if (this.selected_eq.length == 0) {
							document.getElementById('least_one_list_nm').style.display = 'block';
							document.getElementById('select_region_nm').style.display = 'none';
						}
						else {
							document.getElementById('least_one_list_nm').style.display = 'none';
							document.getElementById('select_region_nm').style.display = 'none';
							this.load_page(3);
							this.Graphics_eqr.clear();
							for (var i = 0; i < this.layerEquipamientos.length; i++) {
								var globalid_selected = "globalid IN (";
								for (j = 0; j < this.selected_eq.length; j++) {
									if (i == this.selected_eq[j].index) {
										globalid_selected += "'" + this.selected_eq[j].features.attributes.globalid + "',";
									}
								}
								globalid_selected += "'')";
								var id_eq;
								for (k = 0; k < this.layerEntidades.length; k++) {
									if (this.layerEquipamientos[i] == this.layerEntidades[k].related_layers_title) {
										id_eq = this.layerEntidades[k].related_layers_id;
									}
								}
								var query = new Query();
								query.where = globalid_selected;
								query.outFields = ["*"];
								query.returnGeometry = true;
								this.map._layers[id_eq].selectFeatures(query, FeatureLayer.SELECTION_NEW, (result_select_graphics) => {
									for (var i = 0; i < result_select_graphics.length; i++) {
										if (result_select_graphics[i].geometry["declaredClass"] == 'esri.geometry.Polyline') {
											var line = new SimpleLineSymbol();
											line.setWidth(5);
											line.setColor(new Color([0, 197, 255, 1]));
											result_select_graphics[i].symbol = line;
											this.Graphics_eq.add(result_select_graphics[i]);
											if (i == result_select_graphics.length - 1) {
												this.Graphics_eqr.clear();
												for (var i = 0; i < result_select_graphics.length; i++) {
													this.Graphics_eqr.add(result_select_graphics[i]);
												}
											}
										} else {
											var line = new SimpleLineSymbol();
											line.setColor(new Color([0, 197, 255, 1]));
											var marker = new SimpleMarkerSymbol();
											marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
											marker.setColor(new Color([0, 197, 255, 0.26]));
											marker.setAngle(0);
											marker.setOutline(line);
											result_select_graphics[i].symbol = marker;
											this.Graphics_eqr.add(result_select_graphics[i]);
										}
									}
									this.Graphics_eqr.redraw();
								}, (rollback_select_graphics) => {
									this.Graphics_eqr.clear();
								});
							}
							this.load_table_selected(this.RelacionSeleccionada);
						}
					}
				});

				//Boton Atras Formulario_2
				document.getElementById('end_form_21_nm').addEventListener('click', () => {
					document.getElementById('geometry_oblig_nm').style.display = 'none';
					document.getElementById('least_one_list_nm').style.display = 'none';
					this.load_page(1);
				});

				//Button Combo Tipo Entidad	
				var combobutton = document.getElementById("selec_entidadd_nm");
				combobutton.addEventListener("change", () => {
					this.Graphics_eqr.clear();
					document.getElementById('combo_one_list_nm').style.display = 'none';
					document.getElementById('select_polygond_nm').style.display = 'none';
					document.getElementById('buscadord_nm').style.display = 'none';
					document.getElementById('equipamientos_listd_nm').style.display = 'none';
					document.getElementById('geometry_obligd_nm').style.display = 'none';
					document.getElementById('select_regiond_nm').style.display = 'none';
					document.getElementById('least_one_listd_nm').style.display = 'none';
					document.getElementById('layer_novisibled_nm').style.display = 'none';
					document.getElementById('layer_nogeometria_nm').style.display = 'none';
					document.getElementById('option1_nm').checked = '';
					document.getElementById('option2_nm').checked = '';
					document.getElementById('option1_nm').disabled = false;
					var EntidadSeleccionada = document.getElementById("selec_entidadd_nm").value;
					if (this.layerTablasD.includes(EntidadSeleccionada) == true) {
						document.getElementById('option1_nm').disabled = true;
						document.getElementById('layer_nogeometria_nm').style.display = 'block';
					}
					if (EntidadSeleccionada == this.config.format_red_recreativa_regulacion) {
						document.getElementById('buscador_textd_nm').placeholder = "Introduzca matricula o nombre red recreativa...";
					} else if (EntidadSeleccionada == this.config.format_grupo_instalacion_recreativa_acceso_regulacion) {
						document.getElementById('buscador_textd_nm').placeholder = "Introduzca nombre acceso...";
					} else {
						document.getElementById('buscador_textd_nm').placeholder = "Introduzca codigo o nombre...";
					}
				});

				//Button TipoBusqueda	
				var tipomapabutton = document.getElementById("option1_nm");
				tipomapabutton.addEventListener("click", () => {
					document.getElementById('buscador_textd_nm').value = "";
					document.getElementById('no_listd_nm').style.display = 'none';
					var EntidadSeleccionada = document.getElementById("selec_entidadd_nm").value;
					if (EntidadSeleccionada == "") {
						document.getElementById('combo_one_list_nm').style.display = 'block';
						document.getElementById('option1_nm').checked = '';
						document.getElementById('geometry_obligd_nm').style.display = 'none';
						document.getElementById('select_regiond_nm').style.display = 'none';
						document.getElementById('least_one_listd_nm').style.display = 'none';
						document.getElementById('layer_novisibled_nm').style.display = 'none';
					} else {
						this.Graphics_eqr.clear();
						document.getElementById('geometry_obligd_nm').style.display = 'none';
						document.getElementById('select_regiond_nm').style.display = 'none';
						document.getElementById('least_one_listd_nm').style.display = 'none';
						document.getElementById('layer_novisibled_nm').style.display = 'none';
						document.getElementById('select_polygond_nm').style.display = 'block';
						document.getElementById('buscadord_nm').style.display = 'none';
						document.getElementById('equipamientos_listd_nm').innerHTML = '';
						document.getElementById('equipamientos_listd_nm').style.display = 'none';
					}
				});

				var tipobusquedabutton = document.getElementById("option2_nm");
				tipobusquedabutton.addEventListener("click", () => {
					var EntidadSeleccionada = document.getElementById("selec_entidadd_nm").value;
					if (EntidadSeleccionada == "") {
						document.getElementById('combo_one_list_nm').style.display = 'block';
						document.getElementById('option2_nm').checked = '';
						document.getElementById('geometry_obligd_nm').style.display = 'none';
						document.getElementById('select_regiond_nm').style.display = 'none';
						document.getElementById('least_one_listd_nm').style.display = 'none';
						document.getElementById('layer_novisibled_nm').style.display = 'none';
						document.getElementById('layer_nogeometria_nm').style.display = 'none';
					} else {
						this.Graphics_eqr.clear();
						document.getElementById('geometry_obligd_nm').style.display = 'none';
						document.getElementById('select_regiond_nm').style.display = 'none';
						document.getElementById('least_one_listd_nm').style.display = 'none';
						document.getElementById('layer_novisibled_nm').style.display = 'none';
						document.getElementById('select_polygond_nm').style.display = 'none';
						document.getElementById('layer_nogeometria_nm').style.display = 'none';
						document.getElementById('buscadord_nm').style.display = 'flex';
						document.getElementById('equipamientos_listd_nm').innerHTML = '';
						this.rellenar_entidades();
					}
				});

				//Text Buscador Documentos
				var buscador_f2 = document.getElementById('buscador_textd_nm');
				buscador_f2.addEventListener('input', () => {
					this.rellenar_entidades();
					this.marcar_todos = false;
				});

				//Boton Seleccionar Todos
				var b_select_all = document.getElementById('select_alld_nm');
				b_select_all.addEventListener('click', () => {
					if (this.marcar_todos == false) {
						for (var i = 0; i < this.elementos_eq.length; i++) {
							var n = i.toString();
							var id = 'check_btn_d_' + n;
							if (document.getElementById(id) != null) {
								document.getElementById(id).checked = true;
							}
						}
						for (var i = 0; i < this.Graphics_eqr.graphics.length; i++) {
							if (this.Graphics_eqr.graphics[i].geometry.__proto__.declaredClass == 'esri.geometry.Polyline') {
								var line = new SimpleLineSymbol();
								line.setWidth(5);
								line.setColor(new Color([0, 197, 255, 1]));
								this.Graphics_eqr.graphics[i].setSymbol(line)
							} else {
								var line = new SimpleLineSymbol();
								line.setColor(new Color([0, 197, 255, 1]));
								var marker = new SimpleMarkerSymbol();
								marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
								marker.setColor(new Color([0, 197, 255, 0.26]));
								marker.setOutline(line);
								this.Graphics_eqr.graphics[i].setSymbol(marker);
							}
						}
						this.marcar_todos = true;
					} else {
						for (var i = 0; i < this.elementos_eq.length; i++) {
							var n = i.toString();
							var id = 'check_btn_d_' + n;
							if (document.getElementById(id) != null) {
								document.getElementById(id).checked = false;
							}
						}
						for (var i = 0; i < this.Graphics_eqr.graphics.length; i++) {
							this.Graphics_eqr.graphics[i].setSymbol(null)
						}
						this.marcar_todos = false;
					}
				});

				//Boton Siguiente Formulario_21
				var button_f21 = document.getElementById('end_form_213_nm');
				button_f21.addEventListener('click', () => {
					this.selected_eq = [];
					for (var i = 0; i < this.elementos_eq.length; i++) {
						var n = i.toString();
						var id = 'check_btn_d_' + n;
						if (document.getElementById(id) != null && document.getElementById(id).checked == true) {
							this.selected_eq.push(this.elementos_eq[i]);
						}
					}
					if (this.selected_eq.length == 0) {
						document.getElementById('least_one_listd_nm').style.display = 'block';
						document.getElementById('select_regiond_nm').style.display = 'none';
					}
					else {
						document.getElementById('least_one_listd_nm').style.display = 'none';
						document.getElementById('select_regiond_nm').style.display = 'none';
						this.load_page(3);
						var EntidadSeleccionada = document.getElementById("selec_entidadd_nm").value;					  
		    		if (this.layerGeometriaD.includes(EntidadSeleccionada) == true){
							this.Graphics_eqr.clear();
							for (var i = 0; i < this.layerEquipamientos.length; i++) {
								var globalid_selected = "globalid IN (";
								for (j = 0; j < this.selected_eq.length; j++) {
									if (i == this.selected_eq[j].index) {
										globalid_selected += "'" + this.selected_eq[j].features.attributes.globalid + "',";
									}
								}
								globalid_selected += "'')";
								var id_eq;
								for (k = 0; k < this.layerEntidades.length; k++) {
									if (this.layerEquipamientos[i] == this.layerEntidades[k].related_layers_title) {
										id_eq = this.layerEntidades[k].related_layers_id;
									}
								}
								var query = new Query();
								query.where = globalid_selected;
								query.outFields = ["*"];
								query.returnGeometry = true;
								this.map._layers[id_eq].selectFeatures(query, FeatureLayer.SELECTION_NEW, (result_select_graphics) => {
									for (var i = 0; i < result_select_graphics.length; i++) {
										if (result_select_graphics[i].geometry["declaredClass"] == 'esri.geometry.Polyline') {
											var line = new SimpleLineSymbol();
											line.setWidth(5);
											line.setColor(new Color([0, 197, 255, 1]));
											result_select_graphics[i].symbol = line;
											this.Graphics_eq.add(result_select_graphics[i]);
											if (i == result_select_graphics.length - 1) {
												this.Graphics_eqr.clear();
												for (var i = 0; i < result_select_graphics.length; i++) {
													this.Graphics_eqr.add(result_select_graphics[i]);
												}
											}
										} else {
											var line = new SimpleLineSymbol();
											line.setColor(new Color([0, 197, 255, 1]));
											var marker = new SimpleMarkerSymbol();
											marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
											marker.setColor(new Color([0, 197, 255, 0.26]));
											marker.setAngle(0);
											marker.setOutline(line);
											result_select_graphics[i].symbol = marker;
											this.Graphics_eqr.add(result_select_graphics[i]);
										}
									}
									this.Graphics_eqr.redraw();
								}, (rollback_select_graphics) => {
									this.Graphics_eqr.clear();
								});
							}
						}
						this.load_table_selected(this.RelacionSeleccionada);
					}
				});

				//Boton Atras Formulario_21
				document.getElementById('end_form_211_nm').addEventListener('click', () => {
					this.load_page(1);
				});

				//Boton Siguiente Formulario_3
				var button_f3 = document.getElementById('end_form_34_nm');
				button_f3.addEventListener('click', () => {
					this.selected_relacion_f = [];
					for (var i = 0; i < this.selected_relacion.length; i++) {
						var n = i.toString();
						var id = 'check_btn_relacion_f_' + n;
						if (document.getElementById(id).checked == true) {
							this.selected_relacion_f.push(this.selected_relacion[i]);
						}
					}
					this.selected_eq_f = [];
					for (var i = 0; i < this.selected_eq.length; i++) {
						var n = i.toString();
						var id = 'check_btn_f_' + n;
						if (document.getElementById(id).checked == true) {
							this.selected_eq_f.push(this.selected_eq[i]);
						}
					}
					if (this.selected_relacion_f.length == 0) {
						document.getElementById('one_list_relacion_f_nm').style.display = 'block';
					} else if (this.selected_eq_f.length == 0) {
						document.getElementById('one_list_equipamiento_f_nm').style.display = 'block';
					} else {
						document.getElementById('one_list_relacion_f_nm').style.display = 'none';
						document.getElementById('one_list_equipamiento_f_nm').style.display = 'none';
						this.load_page(4);
					}
				});

				//Boton Atras Formulario_3
				document.getElementById('end_form_32_nm').addEventListener('click', () => {
					if (this.RelacionSeleccionada == this.config.documento) {
						this.load_page(5);
					} else {
						this.load_page(2);					}
					var EntidadSeleccionada = document.getElementById("selec_entidadd_nm").value;					  
		    	if (this.RelacionSeleccionada != this.config.documento || (this.RelacionSeleccionada == this.config.documento && this.layerGeometriaD.includes(EntidadSeleccionada) == true)){
						this.tablaPos_base = this.tablaPos_ini;
						this.graphics_globalid = [];
						for (var l = 0; l < this.Graphics_eqr.graphics.length; l++) {
							this.graphics_globalid.push(this.Graphics_eqr.graphics[l].attributes.globalid);
						}
						this.Graphics_eqr.clear();
						for (var i = 0; i < this.layerEquipamientos.length; i++) {
							var globalid_selected = "globalid IN (";
							for (j = 0; j < this.elementos_eq.length; j++) {
								if (i == this.elementos_eq[j].index) {
									globalid_selected += "'" + this.elementos_eq[j].features.attributes.globalid + "',";
								}
							}
							globalid_selected += "'')";
							var id_eq;
							for (k = 0; k < this.layerEntidades.length; k++) {
								if (this.layerEquipamientos[i] == this.layerEntidades[k].related_layers_title) {
									id_eq = this.layerEntidades[k].related_layers_id;
								}
							}
							var query = new Query();
							query.where = globalid_selected;
							query.outFields = ["*"];
							query.returnGeometry = true;							
							this.map._layers[id_eq].selectFeatures(query, FeatureLayer.SELECTION_NEW, (result_select_graphics) => {
								for (var i = 0; i < result_select_graphics.length; i++) {
									if (result_select_graphics[i].geometry["declaredClass"] == 'esri.geometry.Polyline') {
										if (this.graphics_globalid.includes(result_select_graphics[i].attributes.globalid) == false) {
											result_select_graphics[i].symbol = null;
											this.Graphics_eqr.add(result_select_graphics[i]);
										} else {
											var line = new SimpleLineSymbol();
											line.setWidth(5);
											line.setColor(new Color([0, 197, 255, 1]));
											result_select_graphics[i].symbol = line;
											this.Graphics_eqr.add(result_select_graphics[i]);
										}
									} else {
										if (this.graphics_globalid.includes(result_select_graphics[i].attributes.globalid) == false) {
											result_select_graphics[i].symbol = null;
											this.Graphics_eqr.add(result_select_graphics[i]);
										} else {
											var line = new SimpleLineSymbol();
											line.setColor(new Color([0, 197, 255, 1]));
											var marker = new SimpleMarkerSymbol();
											marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
											marker.setColor(new Color([0, 197, 255, 0.26]));
											marker.setAngle(0);
											marker.setOutline(line);
											result_select_graphics[i].symbol = marker;
											this.Graphics_eqr.add(result_select_graphics[i]);
										}
									}
								}
								this.Graphics_eqr.redraw();
							}, (rollback_select_graphics) => {
								this.Graphics_eqr.clear();
							});
						}
					}
				});

				//Boton Finalizar y Guardar Formulario 4
				var save_button = document.getElementById('end_form_44_nm');
				save_button.addEventListener('click', () => {
					document.getElementById('btn_at_fi_4_nm').style.display = 'none';
					this.create_new_relacion();
				});

				//Boton Atras Formulario 4
				var save_button = document.getElementById('end_form_43_nm');
				save_button.addEventListener('click', () => {
					document.getElementById('applySuccess').style.display = 'none';
					document.getElementById('applyError').style.display = 'none';
					document.getElementById('end_nm').style.display = 'none';
					this.load_page(3);
				});

				//Boton Volver al inicio
				var end_btn = document.getElementById('end_nm');
				end_btn.addEventListener('click', () => {
					this.go_inicio();
				});

				//Botones dialogo de salida
				document.getElementById('confirm_button_exit_nm').addEventListener('click', () => {
					document.getElementById('dialogo_exit_nm').style.display = 'none';
					this.go_inicio();
				});
				document.getElementById('cancel_button_exit_nm').addEventListener('click', function () {
					document.getElementById('dialogo_exit_nm').style.display = 'none';
				});


			},
			rellenar_relacion: function (tipoRelacion) {
				var resultados = [];
				var consultas = [];
				var query = new Query();
				var valor = document.getElementById('buscador_text_nm').value.toUpperCase();
				if (tipoRelacion == this.config.red_recreativa) {
					if (valor == null) {
						query.where = "1=1";
					} else {
						query.where = "UPPER(matricula) like '%" + valor + "%' OR UPPER(nombre) like '%" + valor + "%'";
					}
					query.orderByFields = ["matricula", "nombre"];
					this.url_relacion = this.url_red_recreativa;
				} else if (tipoRelacion == this.config.red_hidraulica) {
					if (valor == null) {
						query.where = "1=1";
					} else {
						query.where = "UPPER(codigo) like '%" + valor + "%' OR UPPER(nombre) like '%" + valor + "%' OR UPPER(codigo_gestion) like '%" + valor + "%'";
					}
					query.orderByFields = ["codigo", "nombre"];
					this.url_relacion = this.url_red_hidraulica;
				} else if (tipoRelacion == this.config.barrera) {
					if (valor == null) {
						query.where = "1=1";
					} else {
						query.where = "UPPER(codigo) like '%" + valor + "%' OR UPPER(nombre) like '%" + valor + "%' OR UPPER(codigo_gestion) like '%" + valor + "%'";
					}
					query.orderByFields = ["codigo", "nombre"];
					this.url_relacion = this.url_barrera;
				} else if (tipoRelacion == this.config.documento) {
					if (valor == null) {
						query.where = "1=1";
					} else {
						query.where = "UPPER(codigo) like '%" + valor + "%' OR UPPER(nombre) like '%" + valor + "%'";
					}
					query.orderByFields = ["codigo", "nombre"];
					this.url_relacion = this.url_documento;
				} else {
					query.where = "1=1";
				}
				query.outFields = ["*"];
				var query_elements = new QueryTask(this.url_relacion);
				consultas.push(query_elements.execute(query));

				Promise.all(consultas).then(results => {
					var all_items = [];
					for (var i = 0; i < results.length; i++) {
						resultados.push(results[i].features);
					}
					for (var i = 0; i < resultados.length; i++) {
						for (var j = 0; j < resultados[i].length; j++) {
							all_items.push(new this.Entidad(resultados[i][j], i));
						}
					}
					this.elementos_relacion = all_items;
					this.load_table_relacion(tipoRelacion);
				}).catch(error => {
					document.getElementById('relacion_list_nm').innerHTML = '<p class="t2_red">Se produjo un error, al cargar los datos de Redes recreativas</p>';
				})
			},
			load_table_relacion: function (tipoRelacion) {
				var htmlToAdd = '';
				if (this.elementos_relacion.length == 0) {
					document.getElementById('relacion_list_nm').style.display = 'none';
					document.getElementById('no_list_nm').style.display = 'block';
				} else {
					document.getElementById('relacion_list_nm').style.display = 'block';
					document.getElementById('no_list_nm').style.display = 'none';
					htmlToAdd += '<ul class="list-group">';
					for (var i = 0; i < this.elementos_relacion.length; i++) {
						if (tipoRelacion == this.config.red_recreativa) {
							htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_relacion_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_relacion[i].features.attributes.matricula + " | " + this.elementos_relacion[i].features.attributes.nombre + "</span></li>";
						} else if (tipoRelacion == this.config.red_hidraulica) {
							htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_relacion_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_relacion[i].features.attributes.codigo + " | " + this.elementos_relacion[i].features.attributes.codigo_gestion + " | " + this.elementos_relacion[i].features.attributes.nombre + "</span></li>";
						} else if (tipoRelacion == this.config.barrera) {
							htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_relacion_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_relacion[i].features.attributes.codigo + " | " + this.elementos_relacion[i].features.attributes.codigo_gestion + " | "  + this.elementos_relacion[i].features.attributes.nombre + "</span></li>";						
						} else if (tipoRelacion == this.config.documento) {
							htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_relacion_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_relacion[i].features.attributes.codigo + " | " + this.elementos_relacion[i].features.attributes.nombre + "</span></li>";
						} else {
							htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_relacion_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_relacion[i].features.attributes.globalid + "</span></li>";
						}
					}
					htmlToAdd += '</ul>';
					document.getElementById('relacion_list_nm').innerHTML = htmlToAdd;
				}
			},

			rellenar_entidades: function () {
				//Obtener la entidad seleccionada
				var EntidadSeleccionada = document.getElementById("selec_entidadd_nm").value;
				document.getElementById('equipamientos_listd_nm').style.display = 'none';
				document.getElementById('no_listd_nm').style.display = 'none';
				document.getElementById('equipamientos_listd_nm').innerHTML = '';
				var resultados = [];
				var consultas = [];
				this.elementos_eq = [];
				this.layerEquipamientos = [];
				var url_capa = "";
				var id_capa = "";				
				for (var i = 0; i < this.layerEntidades.length; i++) {
					if (this.layerEntidades[i].related_layers_title == EntidadSeleccionada) {
						url_capa = this.layerEntidades[i].related_layers_url;
						id_capa = this.layerEntidades[i].related_layers_id;
						this.layerEquipamientos.push(this.layerEntidades[i].related_layers_title);
						break;
					}
				}

				if (this.layerGeometriaD.includes(EntidadSeleccionada) == true) {
					if (this.map._layers[id_capa].visible == true) {
						var query = new Query();
						var valor = document.getElementById('buscador_textd_nm').value.toUpperCase();
						if (valor == '') {
							query.where = "1=1";
						} else {
							valor = "%" + valor + "%";							
							query.where = "(UPPER(codigo) LIKE '" + valor + "' OR UPPER(nombre) LIKE '" + valor + "')";													
						}
						query.outFields = ["*"];
						query.orderByFields = ["codigo", "nombre"];
						query.returnGeometry = true;
						var query_elements = new QueryTask(url_capa);
						consultas.push(query_elements.execute(query));

						Promise.all(consultas).then(results => {
							var all_items = [];
							for (var i = 0; i < results.length; i++) {
								resultados.push(results[i].features);
							}
							for (var i = 0; i < resultados.length; i++) {
								for (var j = 0; j < resultados[i].length; j++) {
									all_items.push(new this.Entidad(resultados[i][j], i));
								}
							}
							this.elementos_eq = all_items;

							var globalid_selected = "globalid IN (";
							for (i = 0; i < this.elementos_eq.length; i++) {
								globalid_selected += "'" + this.elementos_eq[i].features.attributes.globalid + "',";
							}
							globalid_selected += "'')";
							var id_eq;
							for (k = 0; k < this.layerEntidades.length; k++) {
								if (this.layerEquipamientos[0] == this.layerEntidades[k].related_layers_title) {
									id_eq = this.layerEntidades[k].related_layers_id;
								}
							}
							var query_g = new Query();
							query_g.where = globalid_selected;
							query_g.outFields = ["*"];
							query_g.outSpatialReference = this.map.spatialReference;
							query_g.returnGeometry = true;
							this.map._layers[id_eq].selectFeatures(query_g, FeatureLayer.SELECTION_NEW, (result_select_graphics) => {
								for (var i = 0; i < result_select_graphics.length; i++) {
									result_select_graphics[i].symbol = null;
									this.Graphics_eqr.add(result_select_graphics[i]);
								}
								this.Graphics_eqr.redraw();
							},
								(rollback_select_graphics) => {
									this.Graphics_eqr.clear();
								});							
							this.load_table_buscador("CodigoNombre");												
						}).catch(error => {
							document.getElementById('equipamientos_listd_nm').innerHTML = '<p class="t2_red">Se produjo un error, al cargar los datos</p>';
						})
					} else {
						document.getElementById('layer_novisibled_nm').style.display = 'block';
					}
				} else {
					var query = new Query();
					if (EntidadSeleccionada == this.config.format_red_recreativa_sistema_senalizacion) {
						var query = new Query();
						var valor = document.getElementById('buscador_textd_nm').value.toUpperCase();
						if (valor == '') {
							query.where = "1=1";
						} else {
							valor = "%" + valor + "%";
							query.where = "(UPPER(codigo) LIKE '" + valor + "' OR UPPER(nombre) LIKE '" + valor + "')";
						}
						query.outFields = ["*"];
						query.orderByFields = ["codigo", "nombre"];
						var query_elements = new QueryTask(url_capa);
						consultas.push(query_elements.execute(query));

						Promise.all(consultas).then(results => {
							var all_items = [];
							for (var i = 0; i < results.length; i++) {
								resultados.push(results[i].features);
							}
							for (var i = 0; i < resultados.length; i++) {
								for (var j = 0; j < resultados[i].length; j++) {
									all_items.push(new this.Entidad(resultados[i][j], i));
								}
							}
							this.elementos_eq = all_items;
							this.load_table_buscador("CodigoNombre");
						}).catch(error => {
							document.getElementById('equipamientos_listd_nm').innerHTML = '<p class="t2_red">Se produjo un error, al cargar los datos</p>';
						})
					} else if (EntidadSeleccionada == this.config.format_red_recreativa_regulacion) {
						var query = new Query();
						query.where = "1=1";
						query.outFields = ["*"];
						query.orderByFields = ["regulacion_id"];
						var query_elements = new QueryTask(url_capa);
						consultas.push(query_elements.execute(query));

						Promise.all(consultas).then(results => {
							var all_items = [];
							for (var i = 0; i < results.length; i++) {
								resultados.push(results[i].features);
							}
							for (var i = 0; i < resultados.length; i++) {
								for (var j = 0; j < resultados[i].length; j++) {
									all_items.push(new this.Entidad(resultados[i][j], i));
								}
							}
							this.elementos_eq = all_items;
							var resultados_red = [];
							var consultas_red = [];
							var query_where_red = "";
							var valor = document.getElementById('buscador_textd_nm').value.toUpperCase();
							var globalid_red = "globalid IN (";
							for (i = 0; i < this.elementos_eq.length; i++) {
								globalid_red += "'" + this.elementos_eq[i].features.attributes.red_recreativa_id + "',";
							}
							globalid_red += "'')";
							if (valor != "") {
								valor = "%" + valor + "%";
								query_where_red += "( UPPER(nombre) LIKE '" + valor + "' OR UPPER(matricula) LIKE '" + valor + "') AND ";
							}
							if (query_where_red == "") {
								query_where_red = globalid_red;
							} else {
								query_where_red += globalid_red;
							}
							var query_red = new Query();
							query_red.where = query_where_red;
							query_red.outFields = ["*"];
							var query_elements_red = new QueryTask(this.url_red_recreativa);
							consultas_red.push(query_elements_red.execute(query_red));
							Promise.all(consultas_red).then(results => {
								var all_items_red = [];
								for (var i = 0; i < results.length; i++) {
									resultados_red.push(results[i].features);
								}
								for (var i = 0; i < resultados_red.length; i++) {
									for (var j = 0; j < resultados_red[i].length; j++) {
										all_items_red.push(new this.Entidad(resultados_red[i][j], i));
									}
								}
								this.elementos_aux = all_items_red;
								this.load_table_buscador(this.config.format_red_recreativa_regulacion);
							}).catch(error => {
								document.getElementById('equipamientos_listd_nm').innerHTML = '<p class="t2_red">Se produjo un error, al cargar los datos</p>';
							})
						}).catch(error => {
							document.getElementById('equipamientos_listd_nm').innerHTML = '<p class="t2_red">Se produjo un error, al cargar los datos</p>';
						})
					} else if (EntidadSeleccionada == this.config.format_grupo_instalacion_recreativa_acceso_regulacion) {
						var query = new Query();
						query.where = "1=1";
						query.outFields = ["*"];
						query.orderByFields = ["regulacion_id"];
						var query_elements = new QueryTask(url_capa);
						consultas.push(query_elements.execute(query));

						Promise.all(consultas).then(results => {
							var all_items = [];
							for (var i = 0; i < results.length; i++) {
								resultados.push(results[i].features);
							}
							for (var i = 0; i < resultados.length; i++) {
								for (var j = 0; j < resultados[i].length; j++) {
									all_items.push(new this.Entidad(resultados[i][j], i));
								}
							}
							this.elementos_eq = all_items;
							var resultados_acceso = [];
							var consultas_acceso = [];
							var query_where_acceso = "";
							var valor = document.getElementById('buscador_textd_nm').value.toUpperCase();
							var globalid_acceso = "globalid IN (";
							for (i = 0; i < this.elementos_eq.length; i++) {
								globalid_acceso += "'" + this.elementos_eq[i].features.attributes.grupo_instala_recrea_acceso_id + "',";
							}
							globalid_acceso += "'')";
							if (valor != "") {
								valor = "%" + valor + "%";
								query_where_acceso += "( UPPER(nombre) LIKE '" + valor + "') AND ";
							}
							if (query_where_acceso == "") {
								query_where_acceso = globalid_acceso;
							} else {
								query_where_acceso += globalid_acceso;
							}
							var query_acceso = new Query();
							query_acceso.where = query_where_acceso;
							query_acceso.outFields = ["*"];
							var query_elements_acceso = new QueryTask(this.url_acceso);
							consultas_acceso.push(query_elements_acceso.execute(query_acceso));
							Promise.all(consultas_acceso).then(results => {
								var all_items_acceso = [];
								for (var i = 0; i < results.length; i++) {
									resultados_acceso.push(results[i].features);
								}
								for (var i = 0; i < resultados_acceso.length; i++) {
									for (var j = 0; j < resultados_acceso[i].length; j++) {
										all_items_acceso.push(new this.Entidad(resultados_acceso[i][j], i));
									}
								}
								this.elementos_aux = all_items_acceso;
								this.load_table_buscador(this.config.format_grupo_instalacion_recreativa_acceso_regulacion);
							}).catch(error => {
								document.getElementById('equipamientos_listd_nm').innerHTML = '<p class="t2_red">Se produjo un error, al cargar los datos</p>';
							})
						}).catch(error => {
							document.getElementById('equipamientos_listd_nm').innerHTML = '<p class="t2_red">Se produjo un error, al cargar los datos</p>';
						})
					} else {
						document.getElementById('equipamientos_listd_nm').innerHTML = '<p class="t2_red">Se produjo un error, al cargar los datos</p>';
					}
				}
			},

			load_table_buscador: function (tipo) {
				this.tablaPos_ini = [];
				if (this.elementos_eq.length == 0) {
					document.getElementById('no_listd_nm').style.display = 'block';
				}
				else {
					document.getElementById('equipamientos_listd_nm').style.display = 'block';
					var ul = DomConstruct.toDom("<ul class='list-group'>");
					DomConstruct.place(ul, 'equipamientos_listd_nm', 'last');
					for (var i = 0; i < this.elementos_eq.length; i++) {
						if (tipo == "CodigoNombre") {
							var territorio_tab = '';
							for (var j = 0; j < this.TerritorioValue.length; j++) {
								if (this.TerritorioValue[j] == this.elementos_eq[i].features.attributes.territorio_id) {
									territorio_tab = this.Territorio[j];
								}
							}
							var li = DomConstruct.toDom("<li class='list-group-item'>");
							var input = DomConstruct.toDom("<input type='checkbox' id='check_btn_d_" + i + "'>");
							on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_d_")));
							var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_eq[i].features.attributes.codigo + " | " + this.elementos_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
							on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_d_")));
							DomConstruct.place(input, li, 'last');
							DomConstruct.place(span, li, 'last');
							DomConstruct.place(li, ul, 'last');
							this.tablaPos_ini[i] = new this.Tabla_localizador(i, this.elementos_eq[i].features.attributes.globalid);						
						} else if (tipo == this.config.format_red_recreativa_regulacion) {
							if (this.elementos_aux.length == 0) {
								document.getElementById('equipamientos_listd_nm').style.display = 'none';
								document.getElementById('no_listd_nm').style.display = 'block';
							} else {
								for (var j = 0; j < this.elementos_aux.length; j++) {
									if (this.elementos_eq[i].features.attributes.red_recreativa_id == this.elementos_aux[j].features.attributes.globalid) {
										var li = DomConstruct.toDom("<li class='list-group-item'>");
										var input = DomConstruct.toDom("<input type='checkbox' id='check_btn_d_" + i + "'>");
										var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_aux[j].features.attributes.matricula + " | " + this.elementos_aux[j].features.attributes.nombre + " | " + this.DomRegulacion[this.elementos_eq[i].features.attributes.regulacion_id] + "</span>");
										DomConstruct.place(input, li, 'last');
										DomConstruct.place(span, li, 'last');
										DomConstruct.place(li, ul, 'last');
									}
								}
							}
						} else if (tipo == this.config.format_grupo_instalacion_recreativa_acceso_regulacion) {
							if (this.elementos_aux.length == 0) {
								document.getElementById('equipamientos_listd_nm').style.display = 'none';
								document.getElementById('no_listd_nm').style.display = 'block';
							} else {
								for (var j = 0; j < this.elementos_aux.length; j++) {
									if (this.elementos_eq[i].features.attributes.grupo_instala_recrea_acceso_id == this.elementos_aux[j].features.attributes.globalid) {
										var li = DomConstruct.toDom("<li class='list-group-item'>");
										var input = DomConstruct.toDom("<input type='checkbox' id='check_btn_d_" + i + "'>");
										var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_aux[j].features.attributes.nombre + " | " + this.DomRegulacion[this.elementos_eq[i].features.attributes.regulacion_id] + "</span>");
										DomConstruct.place(input, li, 'last');
										DomConstruct.place(span, li, 'last');
										DomConstruct.place(li, ul, 'last');
									}
								}
							}
						} else {
							var li = DomConstruct.toDom("<li class='list-group-item'>");
							var input = DomConstruct.toDom("<input type='checkbox' id='check_btn_d_" + i + "'>");
							var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_eq[i].features.attributes.globalid + "</span>");
							DomConstruct.place(input, li, 'last');
							DomConstruct.place(span, li, 'last');
							DomConstruct.place(li, ul, 'last');
						}
					}
					this.tablaPos_base = this.tablaPos_ini;
				}
			},

			botonDraw: function () {
				document.getElementById('geometry_oblig_nm').style.display = 'none';
				this.Graphics_eqr.clear();
				this.draw.activate('freehandpolygon');
			},

			// DESPLEGAR DROPBOX MULTISELECT
			_desplegarDropbox: function (event) {
				if (event.currentTarget.className == 'MultiselectGrupo') {
					var panel_desplegar = document.getElementById("jimu_dijit_Popup_Grupo");
					if (panel_desplegar.style.display == 'none') {
						panel_desplegar.style.display = 'block';
					} else {
						panel_desplegar.style.display = 'none';
					}
				} else if (event.currentTarget.className == 'MultiselectTerritorio') {
					var panel_desplegar = document.getElementById("jimu_dijit_Popup_Territorio_nm");
					if (panel_desplegar.style.display == 'none') {
						panel_desplegar.style.display = 'block';
					} else {
						panel_desplegar.style.display = 'none';
					}
				}
			},

			//FILTRAR MULTISELECT
			_filtroMultiselect: function (event) {
				var filtro = document.getElementById(event.target.id).value
				for (i = 0; i < event.composedPath()[4].childNodes[3].children[0].children.length; i++) {
					document.getElementById(event.composedPath()[4].childNodes[3].children[0].id).children[i].style.display = "block"
					if (document.getElementById(event.composedPath()[4].childNodes[3].children[0].id).children[i].childNodes[1].innerText.toLowerCase().includes(filtro.toLowerCase())) {
						console.log("Empieza por: " + filtro)
						document.getElementById(event.composedPath()[4].childNodes[3].children[0].id).children[i].style.display = "block"
					} else {
						console.log("No empieza por: " + filtro)
						document.getElementById(event.composedPath()[4].childNodes[3].children[0].id).children[i].style.display = "none"
					}
				}
			},

			//Chequear los MULTISELECT y obtener la lista de valores seleccionados
			_checking: function (event) {
				this.Graphics_eqr.clear();
				document.getElementById('equipamientos_list_nm').innerHTML = '';
				if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Territorio" || document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox checked Territorio") {
					if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Territorio") {
						document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox checked Territorio";
					} else {
						document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox Territorio";
					}

					var conteo = 0;
					document.getElementById("Contador_Territorio_nm").innerHTML = conteo
					for (i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Territorio")[0].childElementCount; i++) {
						var id = "seleccionable_" + i + "_Territorio"
						if (document.getElementById(id).className == "checkInput checkbox checked Territorio") {
							conteo += 1
							document.getElementById("Contador_Territorio_nm").innerHTML = conteo
						}
					}


					//OBTENER VALORES MULTISELECT
					var valores_seleccionados = "(";
					for (i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Territorio")[0].childElementCount; i++) {
						var id = "seleccionable_" + i + "_Territorio"
						if (document.getElementById(id).className == "checkInput checkbox checked Territorio") {
							valores_seleccionados += "'" + document.getElementById(id).getAttribute("data-value") + "'" + ","
						}
					}
					var sin_coma = valores_seleccionados.substring(0, valores_seleccionados.length - 1);
					valores_seleccionados = sin_coma + ")";
					this.valores_seleccionados_Territorio = valores_seleccionados;
				}
			},

			geoEquipamiento: function (event) {
				this.draw.deactivate();
				this.geometry_eq = event.geometry;
				if (this.RelacionSeleccionada == this.config.documento) {
					document.getElementById('geometry_obligd_nm').style.display = 'none';
					document.getElementById('select_regiond_nm').style.display = 'none';
					document.getElementById('least_one_listd_nm').style.display = 'none';
					document.getElementById('layer_novisibled_nm').style.display = 'none';
					document.getElementById('equipamientos_listd_nm').innerHTML = '';
					document.getElementById('select_polygond_nm').innerHTML = "Cargando...";
					//consultar elementos relacionados pillar dato del grupo
					var EntidadSeleccionada = document.getElementById("selec_entidadd_nm").value;
					var resultados = [];
					var consultas = [];
					this.elementos_eq = [];
					this.layerEquipamientos = [];
					var url_capa = "";
					var id_capa = "";
					for (var i = 0; i < this.layerEntidades.length; i++) {
						if (this.layerEntidades[i].related_layers_title == EntidadSeleccionada) {
							url_capa = this.layerEntidades[i].related_layers_url;
							id_capa = this.layerEntidades[i].related_layers_id;
							this.layerEquipamientos.push(this.layerEntidades[i].related_layers_title);
							break;
						}
					}

					if (this.map._layers[id_capa].visible == true) {
						var query = new Query();
						query.geometry = this.geometry_eq;
						query.where = "1=1";
						query.outFields = ["*"];
						query.returnGeometry = true;
						var query_elements = new QueryTask(url_capa);
						consultas.push(query_elements.execute(query));

						Promise.all(consultas).then(results => {
							var all_items_eq = [];
							document.getElementById('select_polygond_nm').innerHTML = "Seleccionar en el Mapa";
							for (var i = 0; i < results.length; i++) {
								resultados.push(results[i].features);
							}
							for (var i = 0; i < resultados.length; i++) {
								for (var j = 0; j < resultados[i].length; j++) {
									all_items_eq.push(new this.Entidad(resultados[i][j], i));
								}
							}
							this.elementos_eq = all_items_eq;
							var globalid_selected = "globalid IN (";
							for (i = 0; i < this.elementos_eq.length; i++) {
								globalid_selected += "'" + this.elementos_eq[i].features.attributes.globalid + "',";
							}
							globalid_selected += "'')";
							var id_eq;
							for (k = 0; k < this.layerEntidades.length; k++) {
								if (this.layerEquipamientos[0] == this.layerEntidades[k].related_layers_title) {
									id_eq = this.layerEntidades[k].related_layers_id;
								}
							}
							var query_g = new Query();
							query_g.where = globalid_selected;
							query_g.outFields = ["*"];
							query_g.returnGeometry = true;
							this.map._layers[id_eq].selectFeatures(query_g, FeatureLayer.SELECTION_NEW, (result_select_graphics) => {
								this.Graphics_eqr.clear();
								for (var i = 0; i < result_select_graphics.length; i++) {
									if (result_select_graphics[i].geometry["declaredClass"] == 'esri.geometry.Polyline') {
										var line = new SimpleLineSymbol();
										line.setWidth(5);
										line.setColor(new Color([0, 197, 255, 1]));
										result_select_graphics[i].symbol = line;
										this.Graphics_eqr.add(result_select_graphics[i]);
										if (i == result_select_graphics.length - 1) {
											this.Graphics_eqr.clear();
											for (var i = 0; i < result_select_graphics.length; i++) {
												this.Graphics_eqr.add(result_select_graphics[i]);
											}
										}
									} else {
										var line = new SimpleLineSymbol();
										line.setColor(new Color([0, 197, 255, 1]));
										var marker = new SimpleMarkerSymbol();
										marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
										marker.setColor(new Color([0, 197, 255, 0.26]));
										marker.setAngle(0);
										marker.setOutline(line);
										result_select_graphics[i].symbol = marker;
										this.Graphics_eqr.add(result_select_graphics[i]);
									}
								}
								this.Graphics_eqr.redraw();
							}
								, (rollback_select_graphics) => {
									this.Graphics_eqr.clear();
								});
							this.load_table();
						}).catch(error => {
							this.Graphics_eqr.clear();
							document.getElementById('select_polygond_nm').innerHTML = "Seleccionar en el Mapa";
							document.getElementById('equipamientos_listd_nm').innerHTML = '<p class="t2_red">Se produjo un error, selecciona la geometrÃ­a nuevamente</p>';
						})
					} else {
						document.getElementById('select_polygond_nm').innerHTML = "Seleccionar en el Mapa";
						document.getElementById('layer_novisibled_nm').style.display = 'block';
					}
				} else {
					document.getElementById('geometry_oblig_nm').style.display = 'none';
					document.getElementById('select_region_nm').style.display = 'none';
					document.getElementById('rlayer_novisible_nm').style.display = 'none';
					document.getElementById('least_one_list_nm').style.display = 'none';
					document.getElementById('equipamientos_list_nm').innerHTML = '';
					document.getElementById('select_polygon_nm').innerHTML = "Cargando...";
					//consultar elementos relacionados pillar dato del grupo
					var resultados = [];
					var consultas = [];
					var where = "1=1";
					this.layerEquipamientos = [];
					if (this.valores_seleccionados_Territorio != ")") {
						var where = "territorio_id in" + this.valores_seleccionados_Territorio
					}

					for (var i = 0; i < this.layerEntidades.length; i++) {
						if (this.map._layers[this.layerEntidades[i].related_layers_id].visible == true) {
							var url_capa = this.layerEntidades[i].related_layers_url;
							var query = new Query();
							query.geometry = this.geometry_eq;
							query.where = where;
							query.outFields = ["*"];
							query.returnGeometry = true
							this.layerEquipamientos.push(this.layerEntidades[i].related_layers_title);
							var query_elements = new QueryTask(url_capa);
							consultas.push(query_elements.execute(query));
						}
					}
					Promise.all(consultas).then(results => {
						var all_items_eq = [];
						document.getElementById('select_polygon_nm').innerHTML = "Seleccionar en el Mapa";
						if (results.length == 0) {
							document.getElementById('equipamientos_list_nm').style.display = 'none';
							document.getElementById('rlayer_novisible_nm').style.display = 'block';
						} else {
							for (var i = 0; i < results.length; i++) {
								resultados.push(results[i].features);
							}
							for (var i = 0; i < resultados.length; i++) {
								for (var j = 0; j < resultados[i].length; j++) {
									all_items_eq.push(new this.Entidad(resultados[i][j], i));
								}
							}
							this.elementos_eq = all_items_eq;
							for (var i = 0; i < this.layerEquipamientos.length; i++) {
								var globalid_selected = "globalid IN (";
								for (j = 0; j < this.elementos_eq.length; j++) {
									if (i == this.elementos_eq[j].index) {
										globalid_selected += "'" + this.elementos_eq[j].features.attributes.globalid + "',";
									}
								}
								globalid_selected += "'')";
								var id_eq;
								for (k = 0; k < this.layerEntidades.length; k++) {
									if (this.layerEquipamientos[i] == this.layerEntidades[k].related_layers_title) {
										id_eq = this.layerEntidades[k].related_layers_id;
									}
								}
								var query = new Query();
								query.where = globalid_selected;
								query.outFields = ["*"];
								query.returnGeometry = true;
								this.map._layers[id_eq].selectFeatures(query, FeatureLayer.SELECTION_NEW, (result_select_graphics) => {
									for (var i = 0; i < result_select_graphics.length; i++) {
										if (result_select_graphics[i].geometry["declaredClass"] == 'esri.geometry.Polyline') {
											var line = new SimpleLineSymbol();
											line.setWidth(5);
											line.setColor(new Color([0, 197, 255, 1]));
											result_select_graphics[i].symbol = line;
											this.Graphics_eqr.add(result_select_graphics[i]);
											if (i == result_select_graphics.length - 1) {
												this.Graphics_eqr.clear();
												for (var i = 0; i < result_select_graphics.length; i++) {
													this.Graphics_eqr.add(result_select_graphics[i]);
												}
											}
										} else {
											var line = new SimpleLineSymbol();
											line.setColor(new Color([0, 197, 255, 1]));
											var marker = new SimpleMarkerSymbol();
											marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
											marker.setColor(new Color([0, 197, 255, 0.26]));
											marker.setAngle(0);
											marker.setOutline(line);
											result_select_graphics[i].symbol = marker;
											this.Graphics_eqr.add(result_select_graphics[i]);
										}
									}
									this.Graphics_eqr.redraw();
								}, (rollback_select_graphics) => {
									this.Graphics_eqr.clear();
								});
							}
							this.load_table();
						}
					}).catch(error => {
						document.getElementById('select_polygon_nm').innerHTML = "Seleccionar en el Mapa";
						document.getElementById('equipamientos_list_nm').innerHTML = '<p class="t2_red">Se produjo un error, selecciona la geometría nuevamente</p>';
					})
				}
			},
			load_table: function () {
				this.tablaPos_ini = [];
				var tipotabla = '';
				var tipoboton = '';
				if (this.RelacionSeleccionada == this.config.documento) {
					tipotabla = 'equipamientos_listd_nm';
					tipoboton = "check_btn_d_";
				} else {
					tipotabla = 'equipamientos_list_nm';
					tipoboton = "check_btn_";
				}

				if (this.elementos_eq.length == 0) {
					document.getElementById(tipotabla).style.display = 'none';
					document.getElementById('select_region_nm').style.display = 'block';
				}
				else {
					document.getElementById(tipotabla).style.display = 'block';
					var ul = DomConstruct.toDom("<ul class='list-group'>");
					DomConstruct.place(ul, tipotabla, 'last');
					for (var i = 0; i < this.elementos_eq.length; i++) {
						if (this.elementos_eq[i].features.attributes.codigo != undefined && this.elementos_eq[i].features.attributes.nombre != undefined) {
							var territorio_tab = '';
							for (var j = 0; j < this.TerritorioValue.length; j++) {
								if (this.TerritorioValue[j] == this.elementos_eq[i].features.attributes.territorio_id) {
									territorio_tab = this.Territorio[j];
								}
							}
							var li = DomConstruct.toDom("<li class='list-group-item'>");
							var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='" + tipoboton + i + "'>");
							on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid, i, tipoboton)));
							if (this.RelacionSeleccionada == this.config.red_hidraulica || this.RelacionSeleccionada == this.config.barrera) {
								var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_eq[i].features.attributes.codigo + " | " + this.elementos_eq[i].features.attributes.codigo_gestion + " | " + this.elementos_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
							} else {
								var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_eq[i].features.attributes.codigo + " | " + this.elementos_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
							}
							on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid, i, tipoboton)));
							DomConstruct.place(input, li, 'last');
							DomConstruct.place(span, li, 'last');
							DomConstruct.place(li, ul, 'last');
							this.tablaPos_ini[i] = new this.Tabla_localizador(i, this.elementos_eq[i].features.attributes.globalid);
						} else {
							if (this.elementos_eq[i].features.attributes.source != undefined) {
								var li = DomConstruct.toDom("<li class='list-group-item'>");
								var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='" + tipoboton + i + "'>");
								on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid, i, tipoboton)));
								var span = DomConstruct.toDom("<span class='t2_tab'>" + " Tramo: " + this.elementos_eq[i].features.attributes.globalid + " | " + this.elementos_eq[i].features.attributes.nombre + "</span>");
								on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid, i, tipoboton)));
								DomConstruct.place(input, li, 'last');
								DomConstruct.place(span, li, 'last');
								DomConstruct.place(li, ul, 'last');
								this.tablaPos_ini[i] = new this.Tabla_localizador(i, this.elementos_eq[i].features.attributes.globalid);
							} else {
								var li = DomConstruct.toDom("<li class='list-group-item'>");
								var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='" + tipoboton + i + "'>");
								on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid, i, tipoboton)));
								var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_eq[i].features.attributes.globalid + "</span>");
								on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid, i, tipoboton)));
								DomConstruct.place(input, li, 'last');
								DomConstruct.place(span, li, 'last');
								DomConstruct.place(li, ul, 'last');
								this.tablaPos_ini[i] = new this.Tabla_localizador(i, this.elementos_eq[i].features.attributes.globalid);
							}
						}
					}
					this.tablaPos_base = this.tablaPos_ini;
				}
			},
			load_table_selected: function (tipoRelacion) {
				var htmlToAdd = '';
				if (this.selected_relacion.length == 0) {
					document.getElementById('relacion_list_f_nm').style.display = 'none';
				} else {
					document.getElementById('relacion_list_f_nm').style.display = 'block';
					htmlToAdd += '<ul class="list-group">';
					for (var i = 0; i < this.selected_relacion.length; i++) {
						if (tipoRelacion == this.config.red_recreativa) {
							htmlToAdd += "<li class='list-group-item'><input type='checkbox' checked='true' id='check_btn_relacion_f_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.selected_relacion[i].features.attributes.matricula + " | " + this.selected_relacion[i].features.attributes.nombre + "</span></li>";
						} else if (tipoRelacion == this.config.red_hidraulica) {
							htmlToAdd += "<li class='list-group-item'><input type='checkbox' checked='true' id='check_btn_relacion_f_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.selected_relacion[i].features.attributes.codigo + " | " + this.selected_relacion[i].features.attributes.nombre + "</span></li>";
						} else if (tipoRelacion == this.config.barrera) {
							htmlToAdd += "<li class='list-group-item'><input type='checkbox' checked='true' id='check_btn_relacion_f_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.selected_relacion[i].features.attributes.codigo + " | " + this.selected_relacion[i].features.attributes.codigo_gestion + " | " + this.selected_relacion[i].features.attributes.nombre + "</span></li>";
						} else if (tipoRelacion == this.config.documento) {
							htmlToAdd += "<li class='list-group-item'><input type='checkbox' checked='true' id='check_btn_relacion_f_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.selected_relacion[i].features.attributes.codigo + " | " + this.selected_relacion[i].features.attributes.nombre + "</span></li>";
						} else {
							htmlToAdd += "<li class='list-group-item'><input type='checkbox' checked='true' id='check_btn_relacion_f_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.selected_relacion[i].features.attributes.globalid + "</span></li>";
						}
					}
					htmlToAdd += '</ul>';
					document.getElementById('relacion_list_f_nm').innerHTML = htmlToAdd;
				}

				this.tablaPos_fin = [];
				document.getElementById('equipamientos_list_f_nm').innerHTML = '';
				if (this.selected_eq.length == 0) {
					document.getElementById('equipamientos_list_f_nm').style.display = 'none';
				}
				else {
					document.getElementById('equipamientos_list_f_nm').style.display = 'block';
					var ul = DomConstruct.toDom("<ul class='list-group'>");
					DomConstruct.place(ul, 'equipamientos_list_f_nm', 'last');
					for (var i = 0; i < this.selected_eq.length; i++) {
						if (tipoRelacion == this.config.documento) {
							var EntidadSeleccionada = document.getElementById("selec_entidadd_nm").value;
							if (this.layerGeometriaD.includes(EntidadSeleccionada) == true || EntidadSeleccionada == this.config.format_red_recreativa_sistema_senalizacion) {
								var li = DomConstruct.toDom("<li class='list-group-item'>");
								var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_f_" + i + "'>");
								on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_f_")));
								var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.selected_eq[i].features.attributes.codigo + " | " + this.selected_eq[i].features.attributes.nombre + "</span>");
								on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_f_")));
								DomConstruct.place(input, li, 'last');
								DomConstruct.place(span, li, 'last');
								DomConstruct.place(li, ul, 'last');
								this.tablaPos_fin[i] = new this.Tabla_localizador(i, this.selected_eq[i].features.attributes.globalid);
							} else if (EntidadSeleccionada == this.config.format_red_recreativa_regulacion) {
								for (var j = 0; j < this.elementos_aux.length; j++) {
									if (this.selected_eq[i].features.attributes.red_recreativa_id == this.elementos_aux[j].features.attributes.globalid) {
										var li = DomConstruct.toDom("<li class='list-group-item'>");
										var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_f_" + i + "'>");
										var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_aux[j].features.attributes.matricula + " | " + this.elementos_aux[j].features.attributes.nombre + " | " + this.DomRegulacion[this.selected_eq[i].features.attributes.regulacion_id] + "</span>");
										DomConstruct.place(input, li, 'last');
										DomConstruct.place(span, li, 'last');
										DomConstruct.place(li, ul, 'last');
									}
								}
							} else if (EntidadSeleccionada == this.config.format_grupo_instalacion_recreativa_acceso_regulacion) {
								for (var j = 0; j < this.elementos_aux.length; j++) {
									if (this.selected_eq[i].features.attributes.grupo_instala_recrea_acceso_id == this.elementos_aux[j].features.attributes.globalid) {
										var li = DomConstruct.toDom("<li class='list-group-item'>");
										var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_f_" + i + "'>");
										var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_aux[j].features.attributes.nombre + " | " + this.DomRegulacion[this.selected_eq[i].features.attributes.regulacion_id] + "</span>");
										DomConstruct.place(input, li, 'last');
										DomConstruct.place(span, li, 'last');
										DomConstruct.place(li, ul, 'last');
									}
								}
							} else {
								var li = DomConstruct.toDom("<li class='list-group-item'>");
								var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_f_" + i + "'>");
								var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.selected_eq[i].features.attributes.globalid + "</span>");
								DomConstruct.place(input, li, 'last');
								DomConstruct.place(span, li, 'last');
								DomConstruct.place(li, ul, 'last');
							}
						} else {
							if (this.selected_eq[i].features.attributes.codigo != undefined && this.selected_eq[i].features.attributes.nombre != undefined) {
								var territorio_tab = '';
								for (var j = 0; j < this.TerritorioValue.length; j++) {
									if (this.TerritorioValue[j] == this.selected_eq[i].features.attributes.territorio_id) {
										territorio_tab = this.Territorio[j];
									}
								}
								var li = DomConstruct.toDom("<li class='list-group-item'>");
								var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_f_" + i + "'>");
								on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_f_")));
								var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.selected_eq[i].features.attributes.codigo + " | " + this.selected_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
								on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_f_")));
								DomConstruct.place(input, li, 'last');
								DomConstruct.place(span, li, 'last');
								DomConstruct.place(li, ul, 'last');
								this.tablaPos_fin[i] = new this.Tabla_localizador(i, this.selected_eq[i].features.attributes.globalid);
							} else {
								if (this.elementos_eq[i].features.attributes.source != undefined) {
									var li = DomConstruct.toDom("<li class='list-group-item'>");
									var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_f_" + i + "'>");
									on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_f_")));
									var span = DomConstruct.toDom("<span class='t2_tab'>" + " Tramo: " + this.selected_eq[i].features.attributes.globalid + " | " + this.selected_eq[i].features.attributes.nombre + "</span>");
									on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_f_")));
									DomConstruct.place(input, li, 'last');
									DomConstruct.place(span, li, 'last');
									DomConstruct.place(li, ul, 'last');
									this.tablaPos_fin[i] = new this.Tabla_localizador(i, this.elementos_eq[i].features.attributes.globalid);
								} else {
									var li = DomConstruct.toDom("<li class='list-group-item'>");
									var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_f_" + i + "'>");
									on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_f_")));
									var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.selected_eq[i].features.attributes.globalid + "</span>");
									on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_f_")));
									DomConstruct.place(input, li, 'last');
									DomConstruct.place(span, li, 'last');
									DomConstruct.place(li, ul, 'last');
									this.tablaPos_fin[i] = new this.Tabla_localizador(i, this.selected_eq[i].features.attributes.globalid);
								}
							}
						}
					}
					this.tablaPos_base = this.tablaPos_fin;
				}
			},
			resaltar_graphics: function (globalid, id_button, tipo) {
				return function () {
					var id = tipo + id_button;
					if (document.getElementById(id) != null && document.getElementById(id).checked == true) {
						for (var i = 0; i < this.Graphics_eqr.graphics.length; i++) {
							if (globalid == this.Graphics_eqr.graphics[i].attributes.globalid) {
								if (this.Graphics_eqr.graphics[i].geometry.__proto__.declaredClass == 'esri.geometry.Polyline') {
									var line = new SimpleLineSymbol();
									line.setWidth(5);
									line.setColor(new Color([230, 0, 0, 1]));
									this.Graphics_eqr.graphics[i].setSymbol(line);
								} else {
									var line = new SimpleLineSymbol();
									line.setColor(new Color([230, 0, 0, 1]));
									var marker = new SimpleMarkerSymbol();
									marker.setOutline(line);
									marker.setColor(new Color([230, 0, 0, 0.25]));
									marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
									this.Graphics_eqr.graphics[i].setSymbol(marker);
								}
							} else {
								for (var j = 0; j < this.tablaPos_base.length; j++) {
									var id = tipo + this.tablaPos_base[j].posicion;
									if (document.getElementById(id).checked == true && this.tablaPos_base[j].globalid == this.Graphics_eqr.graphics[i].attributes.globalid)
										if (this.Graphics_eqr.graphics[i].geometry.__proto__.declaredClass == 'esri.geometry.Polyline') {
											var line = new SimpleLineSymbol();
											line.setWidth(5);
											line.setColor(new Color([0, 197, 255, 1]));
											this.Graphics_eqr.graphics[i].setSymbol(line);
										} else {
											var line = new SimpleLineSymbol();
											line.setColor(new Color([0, 197, 255, 1]));
											var marker = new SimpleMarkerSymbol();
											marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
											marker.setColor(new Color([0, 197, 255, 0.26]));
											marker.setOutline(line);
											this.Graphics_eqr.graphics[i].setSymbol(marker);
										}
								}
							}
						}
					}
				}
			},
			comprobar_graphics: function (globalid, id_button, tipo) {

				return function () {
					var id = tipo + id_button;
					if (document.getElementById(id) != null && document.getElementById(id).checked == true) {
						for (var i = 0; i < this.Graphics_eqr.graphics.length; i++) {
							if (globalid == this.Graphics_eqr.graphics[i].attributes.globalid) {
								if (this.Graphics_eqr.graphics[i].geometry.__proto__.declaredClass == 'esri.geometry.Polyline') {
									var line = new SimpleLineSymbol();
									line.setWidth(5);
									line.setColor(new Color([0, 197, 255, 1]));
									this.Graphics_eqr.graphics[i].setSymbol(line);
								} else {
									var line = new SimpleLineSymbol();
									line.setColor(new Color([0, 197, 255, 1]));
									var marker = new SimpleMarkerSymbol();
									marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
									marker.setColor(new Color([0, 197, 255, 0.26]));
									marker.setOutline(line);
									this.Graphics_eqr.graphics[i].setSymbol(marker);
								}
							}
						}
					} else {
						for (var i = 0; i < this.Graphics_eqr.graphics.length; i++) {
							if (globalid == this.Graphics_eqr.graphics[i].attributes.globalid) {
								this.Graphics_eqr.graphics[i].setSymbol(null);
							}
						}
					}
				}
			},

			getRelationLayerUrlAndName: function (index) {
				let pos = null;
				loop: for (let i = 0; i < this.layerEntidades.length; i++) {
					let le = this.layerEntidades[i];
					if (le.related_layers_title == this.layerEquipamientos[index]) {
						pos = i;
						break loop;
					}
				}
				if (pos != null) {
					return {
						url: this.layerRelations[pos].related_layers_url,
						name: this.layerRelations[pos].related_layers_title
					};
				} else {
					return {
						url: null,
						name: null
					};
				}
			},

			createEditObject: function (name, relFeature, eqFeature) {
				let obj = {};
				switch (name) {
					case 'Documento - arqueta':
						obj = {
							arqueta_id: eqFeature.attributes.globalid,
							documento_id: relFeature.attributes.globalid
						};
						break;
					case 'Regulación red recreativa - documento':
						obj = {
							red_recreativa_regulacion_id: eqFeature.attributes.globalid,
							documento_id: relFeature.attributes.globalid,
							red_recreativa_regulacion_id_ori: eqFeature.attributes.objectid,
							documento_id_ori: relFeature.attributes.objectid
						};
						break;
					case 'Acceso regulación - documento':
						obj = {
							grupo_insta_recre_acc_regu_id: eqFeature.attributes.globalid,
							documento_id: relFeature.attributes.globalid,
							grupo_insta_re_acc_regu_id_or: eqFeature.attributes.objectid,
							documento_id_ori: relFeature.attributes.objectid
						};
						break;
					case 'Documento - carteles':
						obj = {
							cartel_id: eqFeature.attributes.globalid,
							documento_id: relFeature.attributes.globalid
						};
						break;
					case 'Documento - áreas de descanso':
						obj = {
							area_descanso_id: eqFeature.attributes.globalid,
							documento_id: relFeature.attributes.globalid
						};
						break;
					case 'Sistema señalización - documento':
						obj = {
							red_recrea_sist_senalizacion_id: eqFeature.attributes.globalid,
							documento_id: relFeature.attributes.globalid
						};
						break;
					case 'Documento - paneles':
						obj = {
							panel_id: eqFeature.attributes.globalid,
							documento_id: relFeature.attributes.globalid
						};
						break;
					case 'Documento - barreras':
						obj = {
							barrera_id: eqFeature.attributes.globalid,
							documento_id: relFeature.attributes.globalid
						};
						break;
					case 'Documento - instalación recreativa':
						obj = {
							instalacion_recreativa_id: eqFeature.attributes.globalid,
							documento_id: relFeature.attributes.globalid,
							instalacion_recreativa_id_ori: eqFeature.attributes.objectid,
							documento_id_ori: relFeature.attributes.objectid
						};
						break;
					case 'Documento - dotaciones puntuales':
						obj = {
							dotacion_p_id: eqFeature.attributes.globalid,
							documento_id: relFeature.attributes.globalid
						};
						break;
					case 'Documento - depósito':
						obj = {
							deposito_id: eqFeature.attributes.globalid,
							documento_id: relFeature.attributes.globalid
						};
						break;
					case 'Documento - mesas':
						obj = {
							mesa_id: eqFeature.attributes.globalid,
							documento_id: relFeature.attributes.globalid
						};
						break;
					case 'Documento - zonas de aparcamiento':
						obj = {
							zona_aparcamiento_id: eqFeature.attributes.globalid,
							documento_id: relFeature.attributes.globalid
						};
						break;
					case 'Red hidráulica - arqueta':
						obj = {
							red_hidraulica_id: relFeature.attributes.globalid,
							arqueta_id: eqFeature.attributes.globalid,
							red_hidraulica_id_ori: relFeature.attributes.objectid,
							arqueta_id_ori: eqFeature.attributes.objectid
						};
						break;
					case 'Barrera - red hidráulica':
						obj = {
							red_hidraulica_id: relFeature.attributes.globalid,
							barrera_id: eqFeature.attributes.globalid,
							red_hidraulica_id_ori: relFeature.attributes.objectid,
							barrera_id_ori: eqFeature.attributes.objectid
						};
						break;
					case 'Red hidráulica - depósito':
						obj = {
							red_hidraulica_id: relFeature.attributes.globalid,
							deposito_id: eqFeature.attributes.globalid,
							red_hidraulica_id_ori: relFeature.attributes.objectid,
							deposito_id_ori: eqFeature.attributes.objectid
						};
						break;
					case 'Barrera - arqueta':
						obj = {
							barrera_id: relFeature.attributes.globalid,
							arqueta_id: eqFeature.attributes.globalid,
							barrera_id_ori: relFeature.attributes.objectid,
							arqueta_id_ori: eqFeature.attributes.objectid
						};
						break;	
					case 'Barrera - depósito':
						obj = {
							barrera_id: relFeature.attributes.globalid,
							deposito_id: eqFeature.attributes.globalid,
							barrera_id_ori: relFeature.attributes.objectid,
							deposito_id_ori: eqFeature.attributes.objectid
						};
						break;					
					case 'Panel - red recreativa':
						obj = {
							panel_id: eqFeature.attributes.globalid,
							red_recreativa_id: relFeature.attributes.globalid,
							panel_id_ori: eqFeature.attributes.objectid,
							red_recreativa_id_ori: relFeature.attributes.objectid
						};
						break;
					case 'Mesa interpretativa - red recreativa':
						obj = {
							mesa_id: eqFeature.attributes.globalid,
							red_recreativa_id: relFeature.attributes.globalid,
							mesa_id_ori: eqFeature.attributes.objectid,
							red_recreativa_id_ori: relFeature.attributes.objectid
						};
						break;
					case 'Cartel - red recreativa':
						obj = {
							cartel_id: eqFeature.attributes.globalid,
							red_recreativa_id: relFeature.attributes.globalid,
							cartel_id_ori: eqFeature.attributes.objectid,
							red_recreativa_id_ori: relFeature.attributes.objectid
						};
						break;
					case 'Marca - red recreativa':
						obj = {
							marca_id: eqFeature.attributes.globalid,
							red_recreativa_id: relFeature.attributes.globalid,
							marca_id_ori: eqFeature.attributes.objectid,
							red_recreativa_id_ori: relFeature.attributes.objectid
						};
						break;
					case 'Baliza - red recreativa':
						obj = {
							baliza_id: eqFeature.attributes.globalid,
							red_recreativa_id: relFeature.attributes.globalid,
							baliza_id_ori: eqFeature.attributes.objectid,
							red_recreativa_id_ori: relFeature.attributes.objectid
						};
						break;
					case 'Red recreativa - tramo de viario':
						obj = {
							red_recreativa_id: relFeature.attributes.globalid,
							tramo_id: eqFeature.attributes.globalid,
							red_recreativa_id_ori: relFeature.attributes.objectid,
							tramo_id_ori: eqFeature.attributes.objectid
						};
						break;
					case 'Señal direccional - red recreativa':
						obj = {
							direccional_id: eqFeature.attributes.globalid,
							red_recreativa_id: relFeature.attributes.globalid,
							direccional_id_ori: eqFeature.attributes.objectid,
							red_recreativa_id_ori: relFeature.attributes.objectid
						};
						break;
					default:
						obj = null;
						break;
				}
				return obj;
			},

			create_new_relacion: function () {
				document.getElementById('loading_msg_nm').innerHTML = "Guardando relaciones...";
				document.getElementById('loading_msg_nm').className = 't2';

				let arrDef = [];

				for (let i = 0; i < this.selected_relacion_f.length; i++) {
					let relElement = this.selected_relacion_f[i];
					for (let j = 0; j < this.selected_eq_f.length; j++) {
						let eqElement = this.selected_eq_f[j];
						let rlInfo = this.getRelationLayerUrlAndName(eqElement.index);
						let urlCapa = rlInfo.url,
							nameCapa = rlInfo.name;
						let fl = null;
						if (urlCapa != null) {
							fl = new FeatureLayer(urlCapa);
						}
						if (fl != null) {
							let objEdit = this.createEditObject(nameCapa, relElement.features, eqElement.features);
							if (objEdit != null) {
								let g = new Graphic(null, null, objEdit, null);
								let df = fl.applyEdits([g]);
								arrDef.push(df);
							} else {
								throw Error('No se ha podido rellenar objEdit');
							}
						} else {
							throw Error(`No existe la capa ${nameCapa}`);
						}
					}

				}

				Promise.all(arrDef).then((res) => {
					document.getElementById('loading_msg_nm').style.display = 'none';
					document.getElementById('applySuccess').style.display = 'block';
					document.getElementById('end_nm').style.display = 'block';
				}).catch((e) => {
					console.error(e);
					document.getElementById('loading_msg_nm').style.display = 'none';
					document.getElementById('applyError').style.display = 'block';
					document.getElementById('btn_at_fi_4_nm').style.display = 'flex';
				});

			},
			go_inicio: function () {
				this.reseteo();
				this.load_page(0);
			},
			reseteo: function () {
				this.elementos_eq = [];
				this.elementos_aux = [];
				this.selected_eq = [];
				this.selected_eq_f = [];
				this.selected_relacion = [];
				this.selected_relacion_f = [];
				this.elementos_relacion = [];
				this.geometry_eq = '';
				this.Graphics_eqr.clear();
				document.getElementById('relacion_list_nm').innerHTML = '';
				document.getElementById('relacion_list_f_nm').innerHTML = '';
				document.getElementById('equipamientos_list_nm').innerHTML = '';
				document.getElementById('equipamientos_listd_nm').innerHTML = '';
				document.getElementById('equipamientos_list_f_nm').innerHTML = '';
				document.getElementById('buscador_nm').style.display = 'none';
				document.getElementById('buscadord_nm').style.display = 'none';
				document.getElementById('buscador_text_nm').value = "";
				document.getElementById('buscador_textd_nm').value = "";
				document.getElementById('no_list_nm').style.display = 'none';
				document.getElementById('one_list_relacion_nm').style.display = 'none';
				document.getElementById('geometry_oblig_nm').style.display = 'none';
				document.getElementById('rlayer_novisible_nm').style.display = 'none';
				document.getElementById('least_one_list_nm').style.display = 'none';
				document.getElementById('select_region_nm').style.display = 'none';
				document.getElementById('layer_nogeometria_nm').style.display = 'none';
				document.getElementById('combo_one_list_nm').style.display = 'none';
				document.getElementById('layer_novisibled_nm').style.display = 'none';
				document.getElementById('geometry_obligd_nm').style.display = 'none';
				document.getElementById('select_regiond_nm').style.display = 'none';
				document.getElementById('least_one_listd_nm').style.display = 'none';
				document.getElementById('no_listd_nm').style.display = 'none';
				document.getElementById('one_list_relacion_f_nm').style.display = 'none';
				document.getElementById('one_list_equipamiento_f_nm').style.display = 'none';
				document.getElementById('relacion_nm').value = "";
				document.getElementById('selec_entidadd_nm').value = "";
				document.getElementById('option1_nm').checked = '';
				document.getElementById('option2_nm').checked = '';
				document.getElementById('option1_nm').disabled = false;
				document.getElementById('end_nm').style.display = 'none';
				document.getElementById('applySuccess').style.display = 'none';
				document.getElementById('applyError').style.display = 'none';
				document.getElementById('btn_at_fi_4_nm').style.display = 'flex';
				document.getElementById('loading_msg_nm').innerHTML = '';
				//Resetear mutiselect
				//Filtro Territorio
				document.getElementById("Contador_Territorio_nm").innerHTML = 0
				document.getElementById("FiltroTextoTerritorio_nm").value = "";
				this.valores_seleccionados_Territorio = ")";
				document.getElementById("jimu_dijit_Popup_Territorio_nm").style.display = 'none';
				var filtro_texto = document.getElementById("desplegableTerritorio_nm")
				for (i = 1; i < filtro_texto.childNodes.length; i++) {
					filtro_texto.childNodes[i].style.display = "block";
				}
				for (i = 0; i < this.Territorio.length; i++) {
					var seleccionable = document.getElementById("seleccionable_" + i + "_Territorio")
					if (seleccionable.className == "checkInput checkbox checked Territorio") {
						seleccionable.className = "checkInput checkbox Territorio";
					}
				}
			},
			load_page: function (num) {
				document.getElementById('inicio_nm').style.display = 'none';
				document.getElementById('form_1_nm').style.display = 'none';
				document.getElementById('form_2_nm').style.display = 'none';
				document.getElementById('form_21_nm').style.display = 'none';
				document.getElementById('form_3_nm').style.display = 'none';
				document.getElementById('form_4_nm').style.display = 'none';
				document.getElementById('foot_1_nm').className = 'dot';
				document.getElementById('foot_2_nm').className = 'dot';
				document.getElementById('foot_3_nm').className = 'dot';
				document.getElementById('foot_4_nm').className = 'dot';

				if (num > 0 && num <= 5) {
					document.getElementById('footer_nm').style.display = 'block';
				}
				else {
					document.getElementById('footer_nm').style.display = 'none';
					if (num == 0) { document.getElementById('inicio_nm').style.display = 'block'; }
				}
				switch (num) {
					case 1:
						document.getElementById('foot_1_nm').className = 'dot_check';
						document.getElementById('form_1_nm').style.display = 'block';
						break;
					case 2:
						document.getElementById('foot_2_nm').className = 'dot_check';
						document.getElementById('form_2_nm').style.display = 'block';
						break;
					case 5:
						document.getElementById('foot_2_nm').className = 'dot_check';
						document.getElementById('form_21_nm').style.display = 'block';
						break;
					case 3:
						document.getElementById('foot_3_nm').className = 'dot_check';
						document.getElementById('form_3_nm').style.display = 'block';
						break;
					case 4:
						document.getElementById('foot_4_nm').className = 'dot_check';
						document.getElementById('form_4_nm').style.display = 'block';
						break;
				}
			},
			Relations: function (related_layers_url, related_layers_id, related_layers_title) {
				this.related_layers_url = related_layers_url;
				this.related_layers_id = related_layers_id;
				this.related_layers_title = related_layers_title;
			},
			Tabla_localizador: function (posicion, globalid) {
				this.posicion = posicion;
				this.globalid = globalid;
			},
			Entidad: function (features, index) {
				this.features = features;
				this.index = index;
			}
		});
	});


