define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/_base/lang",
    "dojo/dom",
    "dojo/dom-style",
    "dojo/dom-construct",
    "dijit/form/Button",
    "./SearchComponent",
    "dojo/text!./templates/ResultsCategoriesPane.html",
    "dojo/i18n!../nls/strings",
    "./ItemCard",
    "./util"
], function (
    declare,
    array,
    lang,
    dom,
    domStyle,
    domConstruct,
    Button,
    SearchComponent,
    template,
    i18n,
    ItemCard,
    util
) {
    return declare([SearchComponent], {

        i18n: i18n,
        templateString: template,
        categoriesObj: Object(),

        postCreate: function () {
            this.inherited(arguments);
        },

        addItem: function (ItemCard) {
            ItemCard.placeAt(this.categoriesNode);
        },

        addItem2: function (ItemCard, node) {
            ItemCard.placeAt(node);

        },

        destroyItems: function () {
            this.noMatchNode.style.display = "none";
            this.noMatchNode.innerHTML = "";
            this.categoriesNode.innerHTML = "";
        },

        showNoMatch: function () {
            util.setNodeText(this.noMatchNode, i18n.search.resultsCategoriesPane.noMatch);
            this.noMatchNode.style.display = "block";
        },

        alternarContenido: function () {
            var style = domStyle.get(this, "display");
            if (style === "block") {
                domStyle.set(this, "display", "none");
            } else {
                domStyle.set(this, "display", "block");
            }
        },

        // SearchComponent API

        processResults: function (searchResponse) {
            this.destroyItems();
            var results = searchResponse.results;

            if (results && results.length > 0) {
                var idsAdded = util.findLayersAdded(this.getMap(), null).itemIds;
                var objCategorias = Object();
                array.forEach(results, function (res) {
                    if (res.categories !== undefined) {
                        if (res.categories.length > 0) {
                            array.forEach(res.categories, function (cat) {
                                if (!objCategorias.hasOwnProperty(cat)) {
                                    objCategorias[cat] = [];
                                }
                                if (objCategorias[cat].indexOf(res) === -1) {
                                    objCategorias[cat].push(res);
                                }
                            }, this);
                        } else {
                            if (!objCategorias.hasOwnProperty("__nc")) {
                                objCategorias["__nc"] = [];
                            }
                            if (objCategorias["__nc"].indexOf(res) === -1) {
                                objCategorias["__nc"].push(res)
                            }
                        }
                    } else {
                        console.warn("No existe 'categories' en la respuesta")
                    }
                }, this);

                this.categoriesObj = objCategorias;

                for (var [key, values] of Object.entries(objCategorias)) {

                    if (key === "__nc") {
                        var categoria = i18n.search.resultsCategoriesPane.noCat;
                        var cAux = "/Categories/";
                    } else {
                        var categoria = key.replace("/Categories/", "");
                        var cAux = key;
                    }

                    var nodoInicial = this.categoriesNode;

                    var nodoCont = domConstruct.toDom('<div class="search-results ele-cat"></div>');
                    domConstruct.place(nodoCont, nodoInicial, "last");

                    var nodoBoton = domConstruct.toDom(`<a class="jimu-btn">${categoria} (${values.length})</a>`);
                    domConstruct.place(nodoBoton, nodoCont, "last");

                    var nodoItems = domConstruct.toDom(`<div class="search-results"></div>`);
                    domConstruct.place(nodoItems, nodoCont, "last");

                    dojo.connect(nodoBoton, "onclick", nodoItems, this.alternarContenido);

                    array.forEach(values, function (v) {
                        this.addItem2(new ItemCard({
                            item: v,
                            canRemove: (idsAdded.indexOf(v.id) !== -1),
                            resultsPane: this
                        }), nodoItems);
                    }, this);
                }

            } else {
                this.showNoMatch();
            }
        }
    });
});