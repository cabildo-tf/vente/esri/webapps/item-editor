define({
  root: ({
    numPerPage: "Number of items per page",
    scopeOptions: {
      "defaultScope": "Default search scope",
      "labelPlaceholder": "Optional label",
      "MyContent": "Allow My Content",
      "MyOrganization": "Allow My Organization",
      "ArcGISOnline": "Allow ArcGIS Online",
      "Curated": "Allow Curated",
      "CuratedFilter": "Curated filter",
      "livingAtlasExample": "Living atlas example:"
    },
    addFromUrl: {
      caption: "Allow URL"
    },
    addFromFile: {
      caption: "Allow File",
      maxRecordCount: "Maximum records per file"
    },
    searchCategories: {
      caption: "Allow Category Search"
    },
    _default: "Default",
    makeDefault: "Make default"
  }),
  "es": 1
});
